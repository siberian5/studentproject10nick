Таргеты Makefile:

- .vendor-rm – очищает папку vendor-protos
- vendor-proto/google/api – устанавливает proto описания google/googleapis
- vendor-proto/protoc-gen-openapiv2/options - устанавливает proto описания protoc-gen-openapiv2
- vendor-proto/validate - устанавливает proto описания для валидации
- .vendor-proto - устанавливает все vendor-proto зависимости
- bin-deps - устанавливает необходимые бинарные файлы
- protoc-generate - выполняет все вышеприведенные команды
- generate - генерирует все необходимые объекты (в данном случае моки)
- run - запускает приложение
- build - собирает приложение в папку bin со всеми зависимостями (*.json + swagger)
- test - юнит тестирование бизнес слоя
- cover - выводит покрытие юнит тестами в html
- integration - проводит интеграционный тест с тспользованием testcontainers(может долго выполняться)
- sqlc-gen - генерирует файлы для запросов (sqlc.yml)
- migration-status - статус миграций goose для localhost:5432(postgres/password123)
- migration-down - откат миграций goose для localhost:5432(postgres/password123)
- migration-up - накат миграций goose для localhost:5432(postgres/password123)