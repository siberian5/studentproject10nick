package main

import (
	"context"
	"github.com/spf13/pflag"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"route256.ozon.ru/project/loms/internal/app"
	"route256.ozon.ru/project/loms/internal/config"
	"sync"
	"syscall"
)

func main() {

	// get config from cmd
	cfg := parseToConfig()

	// Graceful shutdown
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()
	var wg sync.WaitGroup

	// create app
	app, err := app.NewLOMSApp(cfg)
	if err != nil {
		log.Fatal(err)
	}

	// Starting app
	app.Start()

	// Shutdown goroutine
	wg.Add(1)
	go func() {
		defer wg.Done()

		// Getting context done from signal.Notify
		<-ctx.Done()

		//Stopping app
		app.Stop()
	}()

	wg.Wait()
}

func parseToConfig() *config.AppConfig {

	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	var postgresMasterDsn string
	var postgresSlaveDsn string

	pflag.StringVar(&postgresMasterDsn, "master_dsn", "postgresql://postgres:password123@postgresql-master", "specify a master postgres dsn")
	pflag.StringVar(&postgresSlaveDsn, "slave_dsn", "postgresql://postgres:password123@postgresql-slave", "specify a slave postgres dsn")
	pflag.Parse()

	return &config.AppConfig{
		PostgresMasterDsn: postgresMasterDsn,
		PostgresSlaveDsn:  postgresSlaveDsn,
		Logger:            logger,
	}
}
