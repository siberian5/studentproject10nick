package common

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/jackc/pgx/v5/pgxpool"
	_ "github.com/lib/pq"
	"github.com/pressly/goose/v3"
	"github.com/stretchr/testify/suite"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/network"
	"github.com/testcontainers/testcontainers-go/wait"
	"os"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/repository/loms/order"
	orderPostgres "route256.ozon.ru/project/loms/internal/repository/loms/order/postgres"
	"route256.ozon.ru/project/loms/internal/repository/loms/stock"
	stockPostgres "route256.ozon.ru/project/loms/internal/repository/loms/stock/postgres"
	"route256.ozon.ru/project/loms/internal/service"
	"time"
)

type Suit struct {
	suite.Suite
	svc             *service.LOMSService
	masterContainer testcontainers.Container
	slaveContainer  testcontainers.Container
	masterPool      *pgxpool.Pool
	slavePool       *pgxpool.Pool
}

func (s *Suit) TearDownSuite() {
	s.T().Log("Starting terminate")
	_ = s.masterContainer.Terminate(context.Background())
	_ = s.slaveContainer.Terminate(context.Background())
	s.masterPool.Close()
	s.slavePool.Close()
}

func (s *Suit) SetupSuite() {

	s.T().Log("Setup suite")
	ctx := context.Background()

	// Creating network
	net, err := network.New(ctx,
		network.WithCheckDuplicate(),
		network.WithAttachable(),
		// Makes the network internal only, meaning the host machine cannot access it.
		// Remove or use `network.WithDriver("bridge")` to change the network's mode.
		network.WithDriver("bridge"),
	)
	s.Require().NoError(err)

	// create master container
	s.T().Log("Creating master")

	req := testcontainers.ContainerRequest{
		Image:        "bitnami/postgresql:15.5.0",
		Name:         "postgresql-master",
		ExposedPorts: []string{"5432/tcp"},
		WaitingFor:   wait.ForLog(" database system is ready to accept connections").WithStartupTimeout(time.Minute * 5),
		Env: map[string]string{
			"POSTGRESQL_PGAUDIT_LOG":              "READ,WRITE",
			"POSTGRESQL_LOG_HOSTNAME":             "true",
			"POSTGRESQL_REPLICATION_MODE":         "master",
			"POSTGRESQL_REPLICATION_USER":         "repl_user",
			"POSTGRESQL_REPLICATION_PASSWORD":     "repl_password",
			"POSTGRESQL_PASSWORD":                 "password123",
			"POSTGRESQL_NUM_SYNCHRONOUS_REPLICAS": "1",
		},
		Networks: []string{net.Name},
	}
	pgMaster, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	s.Require().NoError(err)

	s.masterContainer = pgMaster

	// create slave container
	s.T().Log("Creating slave")
	reqSlave := testcontainers.ContainerRequest{
		Image:        "bitnami/postgresql:15.5.0",
		ExposedPorts: []string{"5432/tcp"},
		WaitingFor:   wait.ForLog("database system is ready to accept read-only connections").WithStartupTimeout(time.Minute * 5),
		Env: map[string]string{
			"POSTGRESQL_PASSWORD":             "password123",
			"POSTGRESQL_MASTER_HOST":          "postgresql-master",
			"POSTGRESQL_PGAUDIT_LOG":          "READ,WRITE",
			"POSTGRESQL_LOG_HOSTNAME":         "true",
			"POSTGRESQL_REPLICATION_MODE":     "slave",
			"POSTGRESQL_REPLICATION_USER":     "repl_user",
			"POSTGRESQL_REPLICATION_PASSWORD": "repl_password",
			"POSTGRESQL_MASTER_PORT_NUMBER":   "5432",
		},
		Networks: []string{net.Name},
	}
	pgSlave, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: reqSlave,
		Started:          true,
	})
	s.Require().NoError(err)
	s.slaveContainer = pgSlave

	s.T().Log("Containers created")

	// Get endpoints to db
	masterEndpoint, err := s.masterContainer.Endpoint(context.Background(), "")
	s.Require().NoError(err)

	slaveEndpoint, err := s.slaveContainer.Endpoint(context.Background(), "")
	s.Require().NoError(err)

	// create migrations
	db, err := sql.Open("postgres", fmt.Sprintf("postgresql://postgres:password123@%s/postgres?sslmode=disable", masterEndpoint))
	s.Require().NoError(err)
	err = db.Ping()
	s.Require().NoError(err)

	// setting up goose
	err = goose.SetDialect("pgx")
	s.Require().NoError(err)

	goose.SetBaseFS(os.DirFS("common/migrations"))

	err = goose.Up(db, ".")
	s.Require().NoError(err)

	pool, err := pgxpool.New(ctx, fmt.Sprintf("postgresql://postgres:password123@%s", masterEndpoint))
	s.Require().NoError(err)
	s.masterPool = pool

	poolSlave, err := pgxpool.New(ctx, fmt.Sprintf("postgresql://postgres:password123@%s", slaveEndpoint))
	s.Require().NoError(err)
	s.slavePool = poolSlave

	// creating repos
	orderRepo := orderPostgres.NewOrderRepo(s.masterPool, s.slavePool)
	stockRepo := stockPostgres.NewStockRepo(s.masterPool, s.slavePool)

	s.svc = service.NewLOMSService(orderRepo, stockRepo)

}

func (s *Suit) TestPing() {
	err := s.masterPool.Ping(context.Background())
	s.Require().NoError(err)
	err = s.slavePool.Ping(context.Background())
	s.Require().NoError(err)
}

func (s *Suit) TestCreateOrder() {

	table := []struct {
		Name    string
		Content model.OrderContent
		WantErr error
	}{
		{
			Name: "good stock",
			Content: model.OrderContent{
				UserId: 0,
				Items: []model.Item{
					{
						Id:    1002,
						Count: 10,
					},
				},
			},
			WantErr: nil,
		},
		{
			Name: "bad id",
			Content: model.OrderContent{
				UserId: 0,
				Items: []model.Item{
					{
						Id:    1,
						Count: 10,
					},
				},
			},
			WantErr: stock.ErrInsufficientStocks,
		},
		{
			Name: "bad stock",
			Content: model.OrderContent{
				UserId: 0,
				Items: []model.Item{
					{
						Id:    1002,
						Count: 10000,
					},
				},
			},
			WantErr: stock.ErrInsufficientStocks,
		},
	}
	for _, t := range table {
		_, err := s.svc.Create(context.Background(), t.Content)
		s.Require().ErrorIs(err, t.WantErr)
	}
}

func (s *Suit) TestInfo() {

	id, _ := s.svc.Create(context.Background(), model.OrderContent{
		UserId: 13,
		Items: []model.Item{
			{
				Id:    1002,
				Count: 10,
			},
		},
	})

	table := []struct {
		Name    string
		Id      int64
		Want    model.Order
		WantErr error
	}{
		{
			Name: "good stock",
			Id:   id,
			Want: model.Order{
				Id: id,
				Info: model.OrderContent{
					UserId: 13,
					Items: []model.Item{
						{
							Id:    1002,
							Count: 10,
						},
					},
				},
				Status: "awaiting payment",
			},
			WantErr: nil,
		},
		{
			Name:    "bad id",
			Id:      100500,
			Want:    model.Order{},
			WantErr: order.ErrNoSuchOrder,
		},
	}
	for _, t := range table {
		r, err := s.svc.Info(context.Background(), t.Id)
		s.Require().ErrorIs(err, t.WantErr)
		s.Require().Equal(t.Want, r)
	}
}

func (s *Suit) TestInfoStocks() {

	table := []struct {
		Name    string
		SKU     int64
		WantErr error
	}{
		{
			Name:    "good stock",
			SKU:     1002,
			WantErr: nil,
		},
		{
			Name:    "bad sku",
			SKU:     10030,
			WantErr: stock.ErrNoSuchSKU,
		},
	}
	for _, t := range table {
		_, err := s.svc.StocksInfo(context.Background(), t.SKU)
		s.Require().ErrorIs(err, t.WantErr)
	}
}

func (s *Suit) TestPay() {

	id, _ := s.svc.Create(context.Background(), model.OrderContent{
		UserId: 13,
		Items: []model.Item{
			{
				Id:    28327296,
				Count: 10,
			},
		},
	})

	table := []struct {
		Name    string
		Id      int64
		WantErr error
	}{
		{
			Name:    "good pay",
			Id:      id,
			WantErr: nil,
		},
		{
			Name:    "no such order",
			Id:      100300,
			WantErr: order.ErrNoSuchOrder,
		},
	}
	for _, t := range table {
		err := s.svc.Pay(context.Background(), t.Id)
		s.Require().ErrorIs(err, t.WantErr)
	}
}

func (s *Suit) TestCancel() {

	id, _ := s.svc.Create(context.Background(), model.OrderContent{
		UserId: 13,
		Items: []model.Item{
			{
				Id:    28327296,
				Count: 10,
			},
		},
	})

	table := []struct {
		Name    string
		Id      int64
		WantErr error
	}{
		{
			Name:    "good cancel",
			Id:      id,
			WantErr: nil,
		},
		{
			Name:    "no such order",
			Id:      100300,
			WantErr: order.ErrNoSuchOrder,
		},
	}
	for _, t := range table {
		err := s.svc.Cancel(context.Background(), t.Id)
		s.Require().ErrorIs(err, t.WantErr)
	}
}
