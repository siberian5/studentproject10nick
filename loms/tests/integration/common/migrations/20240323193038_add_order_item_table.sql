-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "order_item"
(
    id       bigserial PRIMARY KEY,
    SKU      bigint not null,
    order_id bigint not null,
    count    int    not null,
    CHECK ( count > 0 ),
    FOREIGN KEY ("order_id") REFERENCES "order" ("id") ON DELETE CASCADE

);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "order_item" CASCADE;
-- +goose StatementEnd
