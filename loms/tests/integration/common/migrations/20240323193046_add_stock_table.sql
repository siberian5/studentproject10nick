-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS stock
(
    SKU         bigint not null PRIMARY KEY,
    total_count bigint not null,
    reserved    bigint not null,
    CONSTRAINT valid_count CHECK (stock.total_count >= stock.reserved)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS stock CASCADE;
-- +goose StatementEnd
