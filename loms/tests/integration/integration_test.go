package integration

import (
	"github.com/stretchr/testify/suite"
	"route256.ozon.ru/project/loms/tests/integration/common"
	"testing"
)

func TestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(common.Suit))
}
