package order_postgres

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgxpool"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/repository/loms/order"
	"sync/atomic"
)

type OrderRepo struct {
	masterConn    *pgxpool.Pool
	masterQueries *Queries
	slaveConn     *pgxpool.Pool
	slaveQueries  *Queries
	counter       uint64
}

func (r *OrderRepo) Create(ctx context.Context, content model.OrderContent) (id int64, err error) {

	// Transaction in master
	tx, err := r.masterConn.Begin(ctx)

	if err != nil {
		return
	}

	// Try to rollback
	defer func() {
		errRollback := tx.Rollback(ctx)
		if errRollback != nil && !errors.Is(errRollback, pgx.ErrTxClosed) {
			err = fmt.Errorf("error while rollback: %w and internal error %w", errRollback, err)
			return
		}
	}()

	// Create new order
	q := r.masterQueries.WithTx(tx)
	id, err = q.Create(ctx, ToCreateParams(content))

	if err != nil {
		return 0, err
	}

	// Insert all items in order to order_item table
	_, err = q.InsertItems(ctx, ToInsertItemsParams(content.Items, id))
	if err != nil {
		return 0, err
	}

	// Commit
	err = tx.Commit(ctx)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *OrderRepo) GetById(ctx context.Context, orderId int64) (model.Order, error) {

	// Get in master/slave
	t, err := r.getReadQuery().GetById(ctx, orderId)
	if err != nil {
		return model.Order{}, err
	}

	// If slice is empty then no such order
	if len(t) > 0 {
		return ToModelOrder(t), nil
	} else {
		return model.Order{}, order.ErrNoSuchOrder
	}

}

func (r *OrderRepo) SetStatus(ctx context.Context, orderId int64, status string) error {
	// just return from master
	return r.masterQueries.SetStatus(ctx, ToSetStatusParams(orderId, status))
}

func (r *OrderRepo) getReadQuery() *Queries {
	if n := atomic.AddUint64(&r.counter, 1); n%2 == 0 {
		return r.masterQueries
	} else {
		return r.slaveQueries
	}
}

func NewOrderRepo(master *pgxpool.Pool, slave *pgxpool.Pool) *OrderRepo {
	return &OrderRepo{
		masterConn:    master,
		masterQueries: New(master),
		slaveConn:     slave,
		slaveQueries:  New(slave),
		counter:       0,
	}
}
