// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.25.0
// source: queries.sql

package order_postgres

import (
	"context"
)

const create = `-- name: Create :one
INSERT INTO "order" (status, user_id)
VALUES ($1, $2)
RETURNING id
`

type CreateParams struct {
	Status string
	UserID int64
}

func (q *Queries) Create(ctx context.Context, arg CreateParams) (int64, error) {
	row := q.db.QueryRow(ctx, create, arg.Status, arg.UserID)
	var id int64
	err := row.Scan(&id)
	return id, err
}

const getById = `-- name: GetById :many
SELECT "order".id, "order".status, "order".user_id, order_item.sku, order_item.count
FROM "order"
         JOIN order_item ON "order".id = order_item.order_id
WHERE "order".id = $1
`

type GetByIdRow struct {
	ID     int64
	Status string
	UserID int64
	Sku    int64
	Count  int32
}

func (q *Queries) GetById(ctx context.Context, id int64) ([]GetByIdRow, error) {
	rows, err := q.db.Query(ctx, getById, id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var items []GetByIdRow
	for rows.Next() {
		var i GetByIdRow
		if err := rows.Scan(
			&i.ID,
			&i.Status,
			&i.UserID,
			&i.Sku,
			&i.Count,
		); err != nil {
			return nil, err
		}
		items = append(items, i)
	}
	if err := rows.Err(); err != nil {
		return nil, err
	}
	return items, nil
}

type InsertItemsParams struct {
	Sku     int64
	OrderID int64
	Count   int32
}

const setStatus = `-- name: SetStatus :exec
UPDATE "order"
SET status = $1
WHERE id = $2
`

type SetStatusParams struct {
	Status string
	ID     int64
}

func (q *Queries) SetStatus(ctx context.Context, arg SetStatusParams) error {
	_, err := q.db.Exec(ctx, setStatus, arg.Status, arg.ID)
	return err
}
