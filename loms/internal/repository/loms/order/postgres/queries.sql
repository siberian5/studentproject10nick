-- name: Create :one
INSERT INTO "order" (status, user_id)
VALUES ($1, $2)
RETURNING id;

-- name: GetById :many
SELECT "order".id, "order".status, "order".user_id, order_item.sku, order_item.count
FROM "order"
         JOIN order_item ON "order".id = order_item.order_id
WHERE "order".id = $1;

-- name: SetStatus :exec
UPDATE "order"
SET status = $1
WHERE id = $2;

-- name: InsertItems :copyfrom
INSERT INTO "order_item" (sku,order_id, count)
VALUES ($1, $2, $3);
