package order_postgres

import (
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/model/status"
)

// ToCreateParams convert domain to create query to db
func ToCreateParams(content model.OrderContent) CreateParams {
	return CreateParams{
		Status: status.New,
		UserID: content.UserId,
	}
}

// ToInsertItemsParams convert domain items to copyFrom structure for insert in db
func ToInsertItemsParams(items []model.Item, orderId int64) []InsertItemsParams {
	t := make([]InsertItemsParams, len(items))
	for i, item := range items {
		t[i] = InsertItemsParams{
			Sku:     item.Id,
			OrderID: orderId,
			Count:   int32(item.Count),
		}
	}

	return t
}

// ToSetStatusParams convert to SetStatus structure
func ToSetStatusParams(orderId int64, status string) SetStatusParams {
	return SetStatusParams{
		Status: status,
		ID:     orderId,
	}
}

// ToModelOrder all selected with join to domain
func ToModelOrder(t []GetByIdRow) model.Order {
	out := make([]model.Item, len(t))

	for i, row := range t {
		out[i] = model.Item{
			Id:    row.Sku,
			Count: uint16(row.Count),
		}
	}

	return model.Order{
		Id: t[0].ID,
		Info: model.OrderContent{
			UserId: t[0].UserID,
			Items:  out,
		},
		Status: t[0].Status,
	}
}
