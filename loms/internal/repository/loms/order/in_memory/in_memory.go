package in_memory

import (
	"context"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/model/status"
	"route256.ozon.ru/project/loms/internal/repository/loms/order"
	"sync"
	"sync/atomic"
)

type Repository struct {
	counter int64
	m       map[int64]model.Order
	mu      sync.RWMutex
}

func NewRepository() *Repository {
	return &Repository{
		counter: 0,
		m:       make(map[int64]model.Order),
		mu:      sync.RWMutex{},
	}
}

func (r *Repository) Create(_ context.Context, content model.OrderContent) (int64, error) {
	r.mu.Lock()
	defer r.mu.Unlock()

	// add to id counter
	id := atomic.AddInt64(&r.counter, 1)

	// create order with new id
	m := model.Order{
		Id:     id,
		Info:   content,
		Status: status.New,
	}

	r.m[id] = m

	return id, nil
}

func (r *Repository) GetById(_ context.Context, orderId int64) (model.Order, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	// get order
	m, ok := r.m[orderId]

	// if no such order
	if !ok {
		return model.Order{}, order.ErrNoSuchOrder
	}

	return m, nil

}

func (r *Repository) SetStatus(_ context.Context, orderId int64, status string) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	// get order
	m, ok := r.m[orderId]

	// if no such order
	if !ok {
		return order.ErrNoSuchOrder
	}

	// change order status
	m.Status = status

	r.m[orderId] = m

	return nil
}
