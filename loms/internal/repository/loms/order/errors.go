package order

import "errors"

var ErrNoSuchOrder = errors.New("no such order")
var ErrDbIsDown = errors.New("db is down")
