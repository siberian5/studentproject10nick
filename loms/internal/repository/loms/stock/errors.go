package stock

import "errors"

var ErrNoSuchSKU = errors.New("no such sku")
var ErrInsufficientStocks = errors.New("insufficient stocks")
