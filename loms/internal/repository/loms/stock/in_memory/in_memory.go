package in_memory

import (
	"context"
	"encoding/json"
	"os"
	"route256.ozon.ru/project/loms/internal/model"
	stock_repo "route256.ozon.ru/project/loms/internal/repository/loms/stock"
	"sync"
)

type OrderMap map[int64]*stock

type stock struct {
	totalCount uint64
	reserved   uint64
	mu         sync.RWMutex
}

type Repository struct {
	m  OrderMap
	mu sync.RWMutex
}

func NewRepository(filename string) (*Repository, error) {

	// read file
	bytes, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	// parse json to slice
	m := make([]struct {
		SKU        int64  `json:"sku"`
		TotalCount uint64 `json:"total_count"`
		Reserved   uint64 `json:"reserved"`
	}, 0)

	err = json.Unmarshal(bytes, &m)
	if err != nil {
		return nil, err
	}

	// initialize start map from slice

	initialMap := make(OrderMap, len(m))

	for _, s := range m {
		initialMap[s.SKU] = &stock{
			totalCount: s.TotalCount,
			reserved:   s.Reserved,
			mu:         sync.RWMutex{},
		}
	}

	return &Repository{
		m:  initialMap,
		mu: sync.RWMutex{},
	}, nil
}

func (r *Repository) Reserve(ctx context.Context, items []model.Item) error {
	r.mu.RLock()
	defer r.mu.RUnlock()

	// for each item reserve stock
	for i, item := range items {
		s, ok := r.m[item.Id]
		if !ok {
			return stock_repo.ErrNoSuchSKU
		}
		s.mu.Lock()

		//if insufficient stocks then rollback (reserve cancel with unpayed flag)
		if s.totalCount-s.reserved < uint64(item.Count) {
			s.mu.Unlock()
			_ = r.ReserveCancel(ctx, items[:i], false)
			return stock_repo.ErrInsufficientStocks
		}
		s.reserved = s.reserved + uint64(item.Count)
		s.mu.Unlock()
	}

	return nil
}

func (r *Repository) ReserveRemove(_ context.Context, items []model.Item) error {
	r.mu.RLock()
	defer r.mu.RUnlock()

	for _, item := range items {
		s := r.m[item.Id]
		s.mu.Lock()
		s.totalCount = s.totalCount - uint64(item.Count)
		s.reserved = s.reserved - uint64(item.Count)
		s.mu.Unlock()
	}

	return nil
}

func (r *Repository) ReserveCancel(_ context.Context, items []model.Item, isPayed bool) error {
	r.mu.RLock()
	defer r.mu.RUnlock()

	for _, item := range items {
		s := r.m[item.Id]
		s.mu.Lock()
		if isPayed {
			s.totalCount = s.totalCount + uint64(item.Count)
		} else {
			s.reserved = s.reserved - uint64(item.Count)
		}

		s.mu.Unlock()
	}

	return nil
}

func (r *Repository) GetBySKU(_ context.Context, id int64) (uint64, error) {
	r.mu.RLock()
	defer r.mu.RUnlock()

	if s, ok := r.m[id]; ok {
		s.mu.RLock()

		count := s.totalCount - s.reserved

		s.mu.RUnlock()

		return count, nil

	} else {
		return 0, stock_repo.ErrNoSuchSKU
	}

}
