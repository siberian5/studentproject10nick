package stock_postgres

import "route256.ozon.ru/project/loms/internal/model"

// ToRawId convert domain item to raw id slice
func ToRawId(items []model.Item) []int64 {
	s := make([]int64, len(items))

	for i, item := range items {
		s[i] = item.Id
	}

	return s
}

// ToUpdateItemParamsReserve convert domain item + Stock to UpdateItemParams to reserve
func ToUpdateItemParamsReserve(item model.Item, stock Stock) UpdateItemParams {
	return UpdateItemParams{
		TotalCount: stock.TotalCount,
		Reserved:   stock.Reserved + int64(item.Count),
		Sku:        stock.Sku,
	}
}

// ToUpdateItemParamsRemove convert domain item + Stock to UpdateItemParams to remove
func ToUpdateItemParamsRemove(item model.Item, stock Stock) UpdateItemParams {
	return UpdateItemParams{
		TotalCount: stock.TotalCount - int64(item.Count),
		Reserved:   stock.Reserved - int64(item.Count),
		Sku:        stock.Sku,
	}
}

// ToUpdateItemParamsCancel convert domain item + Stock to UpdateItemParams to cancel
func ToUpdateItemParamsCancel(item model.Item, stock Stock, isPayed bool) UpdateItemParams {

	// If order payed increase TotalCount else subtract reserved
	if isPayed {
		return UpdateItemParams{
			TotalCount: stock.TotalCount + int64(item.Count),
			Reserved:   stock.Reserved,
			Sku:        stock.Sku,
		}
	} else {
		return UpdateItemParams{
			TotalCount: stock.TotalCount,
			Reserved:   stock.Reserved - int64(item.Count),
			Sku:        stock.Sku,
		}
	}
}
