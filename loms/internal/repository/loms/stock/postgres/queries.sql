-- name: SelectToUpdate :many
SELECT *
from stock
WHERE sku = ANY ($1::bigint[])
ORDER BY sku
    FOR UPDATE;

-- name: GetBySKU :one
SELECT total_count - reserved
from stock
WHERE sku = $1;

-- name: UpdateItem :exec
UPDATE stock
set total_count=$1,
    reserved=$2
WHERE sku = $3;