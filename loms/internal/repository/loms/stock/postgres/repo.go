package stock_postgres

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jackc/pgx/v5/pgxpool"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/repository/loms/stock"
	"sync/atomic"
)

type StockRepo struct {
	masterConn    *pgxpool.Pool
	masterQueries *Queries
	slaveConn     *pgxpool.Pool
	slaveQueries  *Queries
	counter       uint64
}

func (r *StockRepo) Reserve(ctx context.Context, items []model.Item) (err error) {

	// Start transaction in master
	tx, err := r.masterConn.Begin(ctx)
	if err != nil {
		return err
	}

	// try to rollback
	defer func() {
		errRollback := tx.Rollback(ctx)
		if errRollback != nil && !errors.Is(errRollback, pgx.ErrTxClosed) {
			err = fmt.Errorf("error while rollback: %w and internal error %w", errRollback, err)
			return
		}
	}()

	// Create queries with tx
	q := r.masterQueries.WithTx(tx)

	// Select and lock rows
	update, err := q.SelectToUpdate(ctx, ToRawId(items))
	if err != nil {
		return
	}

	// If one/more of rows not in table
	if len(update) != len(items) {
		return stock.ErrInsufficientStocks
	}

	// Update locked rows for reserve
	for i, item := range items {
		err = q.UpdateItem(ctx, ToUpdateItemParamsReserve(item, update[i]))
		if err != nil {
			var pgErr *pgconn.PgError
			if errors.As(err, &pgErr) {
				if pgErr.Code == "23514" && pgErr.ConstraintName == "valid_count" {
					return stock.ErrInsufficientStocks
				}
			}
			return
		}
	}

	// Commit
	err = tx.Commit(ctx)
	if err != nil {
		return
	}

	return nil

}

func (r *StockRepo) ReserveRemove(ctx context.Context, items []model.Item) (err error) {

	// Start transaction in master
	tx, err := r.masterConn.Begin(ctx)
	if err != nil {
		return err
	}

	// try to rollback
	defer func() {
		errRollback := tx.Rollback(ctx)
		if errRollback != nil && !errors.Is(errRollback, pgx.ErrTxClosed) {
			err = fmt.Errorf("error while rollback: %w and internal error %w", errRollback, err)
			return
		}
	}()

	// Create query for tx
	q := r.masterQueries.WithTx(tx)

	// Lock rows for update
	update, err := q.SelectToUpdate(ctx, ToRawId(items))
	if err != nil {
		return err
	}

	// If one/more of rows not in table
	if len(update) != len(items) {
		return stock.ErrInsufficientStocks
	}

	// Update locked rows for remove
	for i, item := range items {
		err := q.UpdateItem(ctx, ToUpdateItemParamsRemove(item, update[i]))
		if err != nil {
			return err
		}
	}

	// Commit
	err = tx.Commit(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (r *StockRepo) ReserveCancel(ctx context.Context, items []model.Item, isPayed bool) (err error) {

	// Start transaction in master
	tx, err := r.masterConn.Begin(ctx)
	if err != nil {
		return err
	}

	// try to rollback
	defer func() {
		errRollback := tx.Rollback(ctx)
		if errRollback != nil && !errors.Is(errRollback, pgx.ErrTxClosed) {
			err = fmt.Errorf("error while rollback: %w and internal error %w", errRollback, err)
			return
		}
	}()

	// Create query for tx
	q := r.masterQueries.WithTx(tx)

	// Lock rows for update
	update, err := q.SelectToUpdate(ctx, ToRawId(items))
	if err != nil {
		return err
	}

	// If one/more of rows not in table
	if len(update) != len(items) {
		return stock.ErrInsufficientStocks
	}

	// Update locked rows by cancel
	for i, item := range items {
		err := q.UpdateItem(ctx, ToUpdateItemParamsCancel(item, update[i], isPayed))
		if err != nil {
			return err
		}
	}

	// Commit
	err = tx.Commit(ctx)
	if err != nil {
		return err
	}

	return nil
}

func (r *StockRepo) GetBySKU(ctx context.Context, id int64) (uint64, error) {

	// Just tru to get from db
	count, err := r.getReadQuery().GetBySKU(ctx, id)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return 0, stock.ErrNoSuchSKU
		}

		return 0, err
	}

	return uint64(count), nil
}

func (r *StockRepo) getReadQuery() *Queries {
	if n := atomic.AddUint64(&r.counter, 1); n%2 == 0 {
		return r.masterQueries
	} else {
		return r.slaveQueries
	}
}

func NewStockRepo(master *pgxpool.Pool, slave *pgxpool.Pool) *StockRepo {
	return &StockRepo{
		masterConn:    master,
		masterQueries: New(master),
		slaveConn:     slave,
		slaveQueries:  New(slave),
		counter:       0,
	}
}
