package status

const (
	New             = "new"
	AwaitingPayment = "awaiting payment"
	Failed          = "failed"
	Canceled        = "canceled"
	Payed           = "payed"
)
