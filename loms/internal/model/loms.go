package model

type Order struct {
	Id     int64
	Info   OrderContent
	Status string
}

type OrderContent struct {
	UserId int64
	Items  []Item
}

type Item struct {
	Id    int64
	Count uint16
}
