package config

import "log/slog"

type AppConfig struct {
	PostgresMasterDsn string
	PostgresSlaveDsn  string
	Logger            *slog.Logger
}
