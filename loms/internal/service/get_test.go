package service

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/repository/loms/order"
	"route256.ozon.ru/project/loms/internal/repository/loms/stock"
	"route256.ozon.ru/project/loms/internal/service/mocks"
	"testing"
)

func TestLOMSService_Info(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name      string
		inputData inputData
		wantData  model.Order
	}{{
		name: "",
		inputData: inputData{
			id: 1,
		},
		wantData: model.Order{
			Id: 1,
			Info: model.OrderContent{
				UserId: 1,
				Items: []model.Item{
					{
						Id:    0,
						Count: 0,
					},
				},
			},
			Status: "",
		},
	}}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			orderRepo.GetByIdMock.Expect(ctx, tt.inputData.id).Return(tt.wantData, nil)

			data, err := svc.Info(ctx, tt.inputData.id)
			require.NoError(t, err)
			require.Equal(t, tt.wantData, data)

		})
	}
}

func TestLOMSService_InfoError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name      string
		inputData inputData
		wantErr   error
	}{{
		name: "",
		inputData: inputData{
			id: 1,
		},
		wantErr: order.ErrNoSuchOrder,
	}}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			orderRepo.GetByIdMock.Expect(ctx, tt.inputData.id).Return(model.Order{}, tt.wantErr)

			_, err := svc.Info(ctx, tt.inputData.id)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}

func TestLOMSService_StockInfo(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name      string
		inputData inputData
		wantData  uint64
	}{{
		name: "",
		inputData: inputData{
			id: 1,
		},
		wantData: 1,
	}}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			stockRepo.GetBySKUMock.Expect(ctx, tt.inputData.id).Return(1, nil)

			count, err := svc.StocksInfo(ctx, tt.inputData.id)
			require.NoError(t, err)
			require.Equal(t, tt.wantData, count)

		})
	}
}

func TestLOMSService_StockInfoError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name      string
		inputData inputData
		wantErr   error
	}{{
		name: "",
		inputData: inputData{
			id: 1,
		},
		wantErr: stock.ErrNoSuchSKU,
	}}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			stockRepo.GetBySKUMock.Expect(ctx, tt.inputData.id).Return(1, tt.wantErr)

			_, err := svc.StocksInfo(ctx, tt.inputData.id)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}
