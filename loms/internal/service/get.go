package service

import (
	"context"
	"route256.ozon.ru/project/loms/internal/model"
)

func (s *LOMSService) Info(ctx context.Context, id int64) (model.Order, error) {
	// just get by id from repo
	return s.OrderRepository.GetById(ctx, id)

}

func (s *LOMSService) StocksInfo(ctx context.Context, sku int64) (uint64, error) {
	// just return count
	return s.StockRepository.GetBySKU(ctx, sku)

}
