package service

import "errors"

var ErrOrderIsNotAwaitingPayment = errors.New("order is not awaiting payment")
var ErrCantCancelOrder = errors.New("order cannot be canceled")
