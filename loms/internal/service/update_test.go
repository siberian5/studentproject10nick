package service

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/model/status"
	"route256.ozon.ru/project/loms/internal/repository/loms/order"
	"route256.ozon.ru/project/loms/internal/repository/loms/stock"
	"route256.ozon.ru/project/loms/internal/service/mocks"
	"testing"
)

func TestLOMSService_Pay(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name      string
		inputData inputData
	}{{
		name: "",
		inputData: inputData{
			id: 1,
		},
	}}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()
			orderRepo.GetByIdMock.Expect(ctx, tt.inputData.id).Return(model.Order{Id: 1, Info: model.OrderContent{
				UserId: 1,
				Items:  []model.Item{},
			}, Status: status.AwaitingPayment}, nil)
			stockRepo.ReserveRemoveMock.Expect(ctx, []model.Item{}).Return(nil)
			orderRepo.SetStatusMock.Expect(ctx, 1, status.Payed).Return(nil)
			err := svc.Pay(ctx, tt.inputData.id)
			require.NoError(t, err)

		})
	}
}

func TestLOMSService_PayError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name          string
		GetByIdErr    error
		ReservePayErr error
		SetStatusErr  error
		wantErr       error
		inputData     inputData
	}{
		{
			name: "",
			inputData: inputData{
				id: 1,
			},
			GetByIdErr: order.ErrDbIsDown,
			wantErr:    order.ErrDbIsDown,
		},
		{
			name: "",
			inputData: inputData{
				id: 1,
			},
			ReservePayErr: stock.ErrNoSuchSKU,
			wantErr:       stock.ErrNoSuchSKU,
		},
		{
			name: "",
			inputData: inputData{
				id: 1,
			},
			SetStatusErr: order.ErrDbIsDown,
			wantErr:      order.ErrDbIsDown,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()
			orderRepo.GetByIdMock.Expect(ctx, tt.inputData.id).Return(model.Order{Id: 1, Info: model.OrderContent{
				UserId: 1,
				Items:  []model.Item{},
			}, Status: status.AwaitingPayment}, tt.GetByIdErr)
			stockRepo.ReserveRemoveMock.Expect(ctx, []model.Item{}).Return(tt.ReservePayErr)
			orderRepo.SetStatusMock.Expect(ctx, 1, status.Payed).Return(tt.SetStatusErr)
			err := svc.Pay(ctx, tt.inputData.id)
			require.ErrorIs(t, err, tt.wantErr)

		})
	}
}

func TestLOMSService_Cancel(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name      string
		inputData inputData
	}{{
		name: "",
		inputData: inputData{
			id: 1,
		},
	}}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()
			orderRepo.GetByIdMock.Expect(ctx, tt.inputData.id).Return(model.Order{Id: 1, Info: model.OrderContent{
				UserId: 1,
				Items:  []model.Item{},
			}}, nil)
			stockRepo.ReserveCancelMock.Expect(ctx, []model.Item{}, false).Return(nil)
			orderRepo.SetStatusMock.Expect(ctx, 1, status.Canceled).Return(nil)
			err := svc.Cancel(ctx, tt.inputData.id)
			require.NoError(t, err)

		})
	}
}

func TestLOMSService_CancelError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		id int64
	}

	table := []struct {
		name             string
		GetByIdErr       error
		ReserveCancelErr error
		SetStatusErr     error
		wantErr          error
		inputData        inputData
	}{
		{
			name: "",
			inputData: inputData{
				id: 1,
			},
			GetByIdErr: order.ErrDbIsDown,
			wantErr:    order.ErrDbIsDown,
		},
		{
			name: "",
			inputData: inputData{
				id: 1,
			},
			ReserveCancelErr: stock.ErrNoSuchSKU,
			wantErr:          stock.ErrNoSuchSKU,
		},
		{
			name: "",
			inputData: inputData{
				id: 1,
			},
			SetStatusErr: order.ErrDbIsDown,
			wantErr:      order.ErrDbIsDown,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()
			orderRepo.GetByIdMock.Expect(ctx, tt.inputData.id).Return(model.Order{Id: 1, Info: model.OrderContent{
				UserId: 1,
				Items:  []model.Item{},
			}}, tt.GetByIdErr)
			stockRepo.ReserveCancelMock.Expect(ctx, []model.Item{}, false).Return(tt.ReserveCancelErr)
			orderRepo.SetStatusMock.Expect(ctx, 1, status.Canceled).Return(tt.SetStatusErr)
			err := svc.Cancel(ctx, tt.inputData.id)
			require.ErrorIs(t, err, tt.wantErr)

		})
	}
}
