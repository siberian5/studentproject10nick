package service

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/model/status"
	"route256.ozon.ru/project/loms/internal/repository/loms/order"
	"route256.ozon.ru/project/loms/internal/repository/loms/stock"
	"route256.ozon.ru/project/loms/internal/service/mocks"
	"testing"
)

func TestLOMSService_Create(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		order model.OrderContent
	}

	table := []struct {
		name      string
		inputData inputData
		wantId    int64
	}{
		{
			name: "No error",
			inputData: inputData{
				order: model.OrderContent{
					UserId: 1,
					Items: []model.Item{{
						Id:    1,
						Count: 1,
					}},
				},
			},
			wantId: 1,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			orderRepo.CreateMock.Expect(ctx, tt.inputData.order).Return(1, nil)
			stockRepo.ReserveMock.Expect(ctx, tt.inputData.order.Items).Return(nil)
			orderRepo.SetStatusMock.Expect(ctx, 1, status.AwaitingPayment).Return(nil)
			id, err := svc.Create(ctx, tt.inputData.order)
			require.NoError(t, err)
			require.Equal(t, tt.wantId, id)
		})
	}
}

func TestLOMSService_CreateError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	orderRepo := mocks.NewOrderRepositoryMock(mc)
	stockRepo := mocks.NewStockRepositoryMock(mc)
	svc := NewLOMSService(orderRepo, stockRepo)

	type inputData struct {
		order model.OrderContent
	}

	table := []struct {
		name         string
		inputData    inputData
		createErr    error
		reserveErr   error
		setStatusErr error
		wantErr      error
	}{
		{
			name: "Create error",
			inputData: inputData{
				order: model.OrderContent{
					UserId: 1,
					Items: []model.Item{{
						Id:    1,
						Count: 1,
					}},
				},
			},
			createErr: order.ErrDbIsDown,
			wantErr:   order.ErrDbIsDown,
		},
		{
			name: "Insufficient stocks",
			inputData: inputData{
				order: model.OrderContent{
					UserId: 1,
					Items: []model.Item{{
						Id:    1,
						Count: 1,
					}},
				},
			},
			reserveErr: stock.ErrInsufficientStocks,
			wantErr:    stock.ErrInsufficientStocks,
		},
		{
			name: "Db is down",
			inputData: inputData{
				order: model.OrderContent{
					UserId: 1,
					Items: []model.Item{{
						Id:    1,
						Count: 1,
					}},
				},
			},
			reserveErr: order.ErrDbIsDown,
			wantErr:    order.ErrDbIsDown,
		},
		{
			name: "Set status error",
			inputData: inputData{
				order: model.OrderContent{
					UserId: 1,
					Items: []model.Item{{
						Id:    1,
						Count: 1,
					}},
				},
			},
			setStatusErr: order.ErrDbIsDown,
			wantErr:      order.ErrDbIsDown,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			orderRepo.CreateMock.Expect(ctx, tt.inputData.order).Return(1, tt.createErr)

			stockRepo.ReserveMock.Expect(ctx, tt.inputData.order.Items).Return(tt.reserveErr)

			if tt.reserveErr == nil {
				orderRepo.SetStatusMock.Expect(ctx, 1, status.AwaitingPayment).Return(tt.setStatusErr)
			} else {
				orderRepo.SetStatusMock.Expect(ctx, 1, status.Failed).Return(tt.setStatusErr)
			}

			_, err := svc.Create(ctx, tt.inputData.order)
			require.ErrorIs(t, tt.wantErr, err)
		})
	}
}
