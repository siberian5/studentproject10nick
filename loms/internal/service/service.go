package service

import (
	"context"
	"route256.ozon.ru/project/loms/internal/model"
)

//go:generate minimock -i route256.ozon.ru/project/loms/internal/service.OrderRepository -s "_mock.go" -o ./mocks/ -g .
type OrderRepository interface {
	Create(ctx context.Context, content model.OrderContent) (int64, error)
	GetById(ctx context.Context, orderId int64) (model.Order, error)
	SetStatus(ctx context.Context, orderId int64, status string) error
}

//go:generate minimock -i route256.ozon.ru/project/loms/internal/service.StockRepository -s "_mock.go" -o ./mocks/ -g .
type StockRepository interface {
	Reserve(ctx context.Context, items []model.Item) error
	ReserveRemove(ctx context.Context, items []model.Item) error
	ReserveCancel(ctx context.Context, items []model.Item, isPayed bool) error
	GetBySKU(ctx context.Context, id int64) (uint64, error)
}

type LOMSService struct {
	OrderRepository
	StockRepository
}

func NewLOMSService(orderRepo OrderRepository, stockRepo StockRepository) *LOMSService {
	return &LOMSService{
		OrderRepository: orderRepo,
		StockRepository: stockRepo,
	}
}
