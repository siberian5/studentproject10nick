package service

import (
	"context"
	"fmt"
	"route256.ozon.ru/project/loms/internal/model"
	"route256.ozon.ru/project/loms/internal/model/status"
)

func (s *LOMSService) Create(ctx context.Context, order model.OrderContent) (int64, error) {

	// creating order
	id, err := s.OrderRepository.Create(ctx, order)
	if err != nil {
		return 0, err
	}

	// reserve items
	err = s.StockRepository.Reserve(ctx, order.Items)
	if err != nil {

		// if we cant reserve then set status failed
		errSetStatus := s.OrderRepository.SetStatus(ctx, id, status.Failed)
		if errSetStatus != nil {
			return 0, fmt.Errorf("error by setting failed status: %w with reserve err %w", errSetStatus, err)
		}
		return 0, err
	}

	// if OK set status to AwaitingPayment
	err = s.OrderRepository.SetStatus(ctx, id, status.AwaitingPayment)
	if err != nil {
		return 0, err
	}

	return id, nil
}
