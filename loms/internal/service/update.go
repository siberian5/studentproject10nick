package service

import (
	"context"
	"route256.ozon.ru/project/loms/internal/model/status"
)

func (s *LOMSService) Pay(ctx context.Context, id int64) error {

	// get order by id
	order, err := s.OrderRepository.GetById(ctx, id)
	if err != nil {
		return err
	}

	// if order is already canceled we cant pay this
	if order.Status != status.AwaitingPayment {
		return ErrOrderIsNotAwaitingPayment
	}

	// change total counts of order items
	err = s.StockRepository.ReserveRemove(ctx, order.Info.Items)
	if err != nil {
		return err
	}

	// changing status
	err = s.OrderRepository.SetStatus(ctx, order.Id, status.Payed)
	if err != nil {
		return err
	}

	return nil
}

func (s *LOMSService) Cancel(ctx context.Context, id int64) error {

	// get order by id
	order, err := s.OrderRepository.GetById(ctx, id)
	if err != nil {
		return err
	}

	// if order is already canceled we cant pay this
	if order.Status == status.Failed || order.Status == status.Canceled {
		return ErrCantCancelOrder
	}

	// delete reserve on order items
	err = s.StockRepository.ReserveCancel(ctx, order.Info.Items, order.Status == status.Payed)
	if err != nil {
		return err
	}

	// changing status
	err = s.OrderRepository.SetStatus(ctx, order.Id, status.Canceled)
	if err != nil {
		return err
	}

	return nil
}
