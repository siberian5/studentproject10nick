package app

import (
	"context"
	"errors"
	"fmt"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/jackc/pgx/v5/pgxpool"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/reflection"
	"log"
	"log/slog"
	"net"
	"net/http"
	api "route256.ozon.ru/project/loms/internal/api/loms"
	"route256.ozon.ru/project/loms/internal/config"
	orderpostgres "route256.ozon.ru/project/loms/internal/repository/loms/order/postgres"
	stockPostgres "route256.ozon.ru/project/loms/internal/repository/loms/stock/postgres"
	usecase "route256.ozon.ru/project/loms/internal/service"
	desc "route256.ozon.ru/project/loms/pkg/api/loms/v1"
	"route256.ozon.ru/project/loms/pkg/interceptors"
	"sync"
	"time"
)

const GRPCPort = 50051
const GatewayPort = 8080

type LOMS struct {
	conn          net.Listener
	connGateway   *grpc.ClientConn
	server        *grpc.Server
	gatewayServer *http.Server
	masterConn    *pgxpool.Pool
	slaveConn     *pgxpool.Pool
	useCase       *usecase.LOMSService
	logger        *slog.Logger
	wg            sync.WaitGroup
}

func NewLOMSApp(cfg *config.AppConfig) (*LOMS, error) {

	ctx := context.Background()

	// create listen post for grpc (don`t need to close conn because grpc server stops it)
	conn, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%d", GRPCPort))
	if err != nil {
		return nil, err
	}

	// create server
	grpcServer := grpc.NewServer(grpc.ChainUnaryInterceptor(
		interceptors.Logger(cfg.Logger),
		interceptors.Recover(cfg.Logger),
		interceptors.Validate,
	))

	// Register reflection of server
	reflection.Register(grpcServer)

	// Create connection to master db
	masterConn, err := pgxpool.New(ctx, cfg.PostgresMasterDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to create conn to master: %w", err)
	}
	err = masterConn.Ping(ctx)
	if err != nil {
		log.Fatal(fmt.Errorf("failed to connect to master: %w", err))
	}
	cfg.Logger.Info("Connection to master is up")

	// Create connection to slave db
	slaveConn, err := pgxpool.New(ctx, cfg.PostgresSlaveDsn)
	if err != nil {
		return nil, fmt.Errorf("failed to create conn to slave: %w", err)
	}
	err = slaveConn.Ping(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to connect to slave: %w", err)
	}
	cfg.Logger.Info("Connection to slave is up")

	// Create order repo with pg
	orderRepo := orderpostgres.NewOrderRepo(masterConn, slaveConn)

	// Create stock repo with pg
	stockRepo := stockPostgres.NewStockRepo(masterConn, slaveConn)

	// create usecases
	useCase := usecase.NewLOMSService(orderRepo, stockRepo)

	// create service (transport layer)
	implementation := api.NewService(useCase, cfg.Logger)

	// register server to out implementation
	desc.RegisterLOMSServer(grpcServer, implementation)

	// dial the gRPC server above to make a gateway connection
	connGateway, err := grpc.Dial(fmt.Sprintf("0.0.0.0:%d", GRPCPort), grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		cfg.Logger.Error(fmt.Errorf("fail to dial: %w", err).Error())
		return nil, err
	}

	// create an HTTP router for service
	gwmux := runtime.NewServeMux()
	client := desc.NewLOMSClient(connGateway)
	err = desc.RegisterLOMSHandlerClient(context.Background(), gwmux, client)
	if err != nil {
		cfg.Logger.Error(fmt.Errorf("fail to RegisterLOMSHandlerClient: %w", err).Error())
		return nil, err
	}

	// create a standard HTTP router
	mux := http.NewServeMux()

	// mount the gRPC HTTP gateway to the root
	mux.Handle("/", gwmux)

	addSwagger(mux)

	// Create server
	server := &http.Server{
		Addr:              fmt.Sprintf("0.0.0.0:%d", GatewayPort),
		Handler:           mux,
		WriteTimeout:      time.Second * 1,
		ReadHeaderTimeout: time.Millisecond * 500,
		IdleTimeout:       time.Second * 5,
	}

	return &LOMS{
		conn:          conn,
		connGateway:   connGateway,
		server:        grpcServer,
		gatewayServer: server,
		masterConn:    masterConn,
		slaveConn:     slaveConn,
		useCase:       useCase,
		logger:        cfg.Logger,
		wg:            sync.WaitGroup{},
	}, nil

}

func (l *LOMS) Start() {

	l.wg.Add(1)
	go func() {
		defer l.wg.Done()

		// start grpc
		l.logger.Info(fmt.Sprintf("Start listening grpc at 0.0.0.0:%d", GRPCPort))
		if err := l.server.Serve(l.conn); err != nil && !errors.Is(grpc.ErrServerStopped, err) {
			l.logger.Error(err.Error())
			return
		}
	}()

	l.wg.Add(1)
	go func() {
		defer l.wg.Done()

		// start a standard HTTP server with the router
		l.logger.Info(fmt.Sprintf("Start listening http at %s", l.gatewayServer.Addr))

		err := l.gatewayServer.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			l.logger.Error(err.Error())
			return
		}
	}()

}

func (l *LOMS) Stop() {
	l.logger.Info("Starting shutdown")

	//stop grpc server
	l.server.GracefulStop()
	l.logger.Info("GRPC server is stopped")

	// Context to close all connections and others
	ctxShutdown, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	err := l.gatewayServer.Shutdown(ctxShutdown)
	if err != nil {
		l.logger.Error(err.Error())
	}
	l.logger.Info("Gateway server is stopped")

	// close gateway grpc connection
	err = l.connGateway.Close()
	if err != nil {
		l.logger.Error(err.Error())
	}
	l.logger.Info("Conn gateway server is closed")

	// close db conns
	l.masterConn.Close()
	l.logger.Info("Master conn is stopped")
	l.slaveConn.Close()
	l.logger.Info("Slave conn is stopped")

	l.wg.Wait()

	l.logger.Info("Shutdown successful")
}

func addSwagger(mux *http.ServeMux) {
	// mount a path to expose the generated OpenAPI specification on disk
	mux.HandleFunc("/docs/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "./api/openapiv2/loms.swagger.json")
	})

	// mount the Swagger UI that uses the OpenAPI specification path above
	mux.Handle("/docs/", http.StripPrefix("/docs/", http.FileServer(http.Dir("./third_party/swagger"))))
}
