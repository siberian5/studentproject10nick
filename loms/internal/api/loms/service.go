package loms

import (
	"context"
	"errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
	"log/slog"
	"route256.ozon.ru/project/loms/internal/model"
	orderRepo "route256.ozon.ru/project/loms/internal/repository/loms/order"
	"route256.ozon.ru/project/loms/internal/repository/loms/stock"
	usecase "route256.ozon.ru/project/loms/internal/service"
	desc "route256.ozon.ru/project/loms/pkg/api/loms/v1"
	"route256.ozon.ru/project/loms/pkg/utils/converter"
)

type UseCase interface {
	Create(ctx context.Context, order model.OrderContent) (int64, error)
	Info(ctx context.Context, id int64) (model.Order, error)
	Pay(ctx context.Context, id int64) error
	Cancel(ctx context.Context, id int64) error
	StocksInfo(ctx context.Context, sku int64) (uint64, error)
}

type Service struct {
	desc.UnimplementedLOMSServer
	UseCase
	log *slog.Logger
}

func NewService(useCase UseCase, log *slog.Logger) *Service {
	return &Service{UnimplementedLOMSServer: desc.UnimplementedLOMSServer{}, UseCase: useCase, log: log}
}

func (s *Service) OrderCreate(ctx context.Context, request *desc.OrderCreateRequest) (*desc.OrderCreateResponse, error) {

	id, err := s.UseCase.Create(ctx, converter.OrderCreateRequestToOrderContent(request))

	if err != nil {
		if errors.Is(err, stock.ErrNoSuchSKU) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		if errors.Is(err, stock.ErrInsufficientStocks) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.FailedPrecondition, err.Error())
		}
		s.log.Error(err.Error())
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &desc.OrderCreateResponse{OrderId: id}, nil
}

func (s *Service) OrderInfo(ctx context.Context, request *desc.OrderInfoRequest) (*desc.OrderInfoResponse, error) {

	order, err := s.UseCase.Info(ctx, request.OrderId)

	if err != nil {

		if errors.Is(err, orderRepo.ErrNoSuchOrder) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		s.log.Error(err.Error())
		return nil, status.Error(codes.Internal, "internal error")
	}

	return converter.OrderToOrderInfoResponse(order), nil
}

func (s *Service) OrderPay(ctx context.Context, request *desc.OrderPayRequest) (*emptypb.Empty, error) {
	err := s.UseCase.Pay(ctx, request.OrderId)

	if err != nil {

		if errors.Is(err, orderRepo.ErrNoSuchOrder) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		if errors.Is(err, usecase.ErrOrderIsNotAwaitingPayment) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		s.log.Error(err.Error())
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &emptypb.Empty{}, nil
}

func (s *Service) OrderCancel(ctx context.Context, request *desc.OrderCancelRequest) (*emptypb.Empty, error) {

	err := s.UseCase.Cancel(ctx, request.OrderId)

	if err != nil {

		if errors.Is(err, orderRepo.ErrNoSuchOrder) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		if errors.Is(err, usecase.ErrCantCancelOrder) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}

		s.log.Error(err.Error())
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &emptypb.Empty{}, nil
}

func (s *Service) StocksInfo(ctx context.Context, request *desc.StocksInfoRequest) (*desc.StocksInfoResponse, error) {

	count, err := s.UseCase.StocksInfo(ctx, request.Sku)

	if err != nil {
		if errors.Is(err, stock.ErrNoSuchSKU) {
			s.log.Debug(err.Error())
			return nil, status.Error(codes.InvalidArgument, err.Error())
		}
		s.log.Error(err.Error())
		return nil, status.Error(codes.Internal, "internal error")
	}

	return &desc.StocksInfoResponse{Count: count}, nil

}
