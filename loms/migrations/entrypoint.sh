#!/bin/bash

DBSTRING="host=postgresql-master user=postgres password=$POSTGRESQL_MASTER_PASSWORD dbname=postgres sslmode=disable"

goose postgres "$DBSTRING" up