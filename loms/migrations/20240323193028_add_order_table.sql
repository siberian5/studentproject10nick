-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS "order"
(
    id      bigserial  PRIMARY KEY,
    status  varchar not null,
    user_id bigint  not null
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE IF EXISTS "order" CASCADE;
-- +goose StatementEnd
