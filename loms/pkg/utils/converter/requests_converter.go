package converter

import (
	"route256.ozon.ru/project/loms/internal/model"
	desc "route256.ozon.ru/project/loms/pkg/api/loms/v1"
)

func OrderCreateRequestToOrderContent(request *desc.OrderCreateRequest) model.OrderContent {

	dtoItems := request.Items
	domainItems := make([]model.Item, len(dtoItems))

	for i := range dtoItems {
		domainItems[i] = model.Item{
			Id:    dtoItems[i].Sku,
			Count: uint16(dtoItems[i].Count),
		}
	}

	return model.OrderContent{
		UserId: request.UserId,
		Items:  domainItems,
	}

}
