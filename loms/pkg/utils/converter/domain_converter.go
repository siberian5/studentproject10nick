package converter

import (
	"route256.ozon.ru/project/loms/internal/model"
	desc "route256.ozon.ru/project/loms/pkg/api/loms/v1"
)

func OrderToOrderInfoResponse(order model.Order) *desc.OrderInfoResponse {

	m := make([]*desc.Item, len(order.Info.Items))

	for i := range order.Info.Items {
		m[i] = &desc.Item{
			Sku:   order.Info.Items[i].Id,
			Count: uint32(order.Info.Items[i].Count),
		}
	}

	return &desc.OrderInfoResponse{
		Status: order.Status,
		UserId: order.Info.UserId,
		Items:  m,
	}
}
