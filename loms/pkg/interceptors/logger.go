package interceptors

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log/slog"
	"time"
)

func Logger(log *slog.Logger) func(context.Context, interface{}, *grpc.UnaryServerInfo, grpc.UnaryHandler) (interface{}, error) {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {

		start := time.Now()

		defer func() {
			log.Debug(fmt.Sprintf("response: method: %v, err: %v, elapsed time: %d mksec", info.FullMethod, err, time.Since(start).Microseconds()))
		}()

		resp, err = handler(ctx, req)

		return
	}
}
