Для запуска интеграционных тестов необходимо выполнить в корне репозитория:
```
make run-all
cd cart/
make e2e
make e2e-allure
```

Таргеты Makefile:

- run – запускает сервис
- build – собирает бинарный файл в папку ./bin
- generate - генерирует все необходимые объекты (в данном случае моки)
- test - юнит тестирование бизнес слоя и репозитория
- cover - выводит покрытие юнит тестами в html
- benchmark - бенчмарк in-memory хранилища
- cyclo - цикломатическая сложность
- cognit - когнитивная сложность
- complexity - цикломатическая и когнитивная сложности
- e2e - end-to-end тест (должен быть поднят докер)
- e2e-allure - end-to-end тесит с выводом в allure html (должен быть установлен allure и в докере подняты образы)
- .vendor-rm – очищает папку vendor-protos
- vendor-proto/google/api – устанавливает proto описания google/googleapis
- vendor-proto/protoc-gen-openapiv2/options - устанавливает proto описания protoc-gen-openapiv2
- vendor-proto/validate - устанавливает proto описания для валидации
- .vendor-proto - устанавливает все vendor-proto зависимости
- bin-deps - устанавливает необходимые бинарные файлы
- protoc-generate - выполняет все вышеприведенные команды, необходимые для генерации grpc