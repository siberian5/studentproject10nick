module route256.ozon.ru/project/cart

go 1.22.0

require (
	github.com/envoyproxy/protoc-gen-validate v1.0.4
	github.com/gojuno/minimock/v3 v3.3.6
	github.com/gookit/validate v1.5.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.19.1
	github.com/ozontech/allure-go/pkg/framework v0.6.29
	github.com/spf13/pflag v1.0.5
	github.com/stretchr/testify v1.9.0
	go.mongodb.org/mongo-driver v1.14.0
	google.golang.org/genproto/googleapis/api v0.0.0-20240314234333-6e1732d8331c
	google.golang.org/grpc v1.62.1
	google.golang.org/protobuf v1.33.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gookit/filter v1.2.1 // indirect
	github.com/gookit/goutil v0.6.15 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/ozontech/allure-go/pkg/allure v0.6.12 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/rogpeppe/go-internal v1.9.0 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.2 // indirect
	github.com/xdg-go/stringprep v1.0.4 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.uber.org/goleak v1.3.0 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	golang.org/x/net v0.20.0 // indirect
	golang.org/x/sync v0.6.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240311132316-a219d84964c2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
