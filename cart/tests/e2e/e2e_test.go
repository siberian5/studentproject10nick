package main

import (
	allure_suite "github.com/ozontech/allure-go/pkg/framework/suite"
	"github.com/stretchr/testify/suite"
	"route256.ozon.ru/project/cart/tests/e2e/item"
	"testing"
)

func TestSuite(t *testing.T) {
	t.Parallel()
	suite.Run(t, new(item.Suit))
}

func TestAllureSuite(t *testing.T) {
	t.Parallel()
	allure_suite.RunSuite(t, new(item.AllureSuit))
}
