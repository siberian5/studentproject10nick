package item

import (
	"fmt"
	"github.com/stretchr/testify/suite"
	"net/http"
)

type Suit struct {
	suite.Suite
	client *http.Client
	url    string
}

const addURL = "user/1/cart/28327296"
const addAnotherURL = "user/1/cart/28347396"
const getURL = "user/1/cart"
const deleteAllURL = "user/1/cart"
const deleteSingleURL = "user/1/cart/28327296"

func (s *Suit) SetupSuite() {

	s.client = http.DefaultClient
	s.url = "http://localhost:8082"
}

// TestHeartbeat Testing heartbeat endpoint
func (s *Suit) TestHeartbeat() {
	response, err := s.client.Get(fmt.Sprintf("%s/heartbeat", s.url))
	s.Require().NoError(err)
	defer response.Body.Close()
	s.Require().Equal(http.StatusOK, response.StatusCode)
}

// TestGetEmpty Testing get cart endpoint when cart is empty (no such user)
func (s *Suit) TestGetEmpty() {
	response, err := s.client.Get(fmt.Sprintf("%s/%s", s.url, getURL))
	s.Require().NoError(err)
	defer response.Body.Close()
	s.Require().Equal(http.StatusNotFound, response.StatusCode)
}

// TestDeleteAllEmpty Testing clear cart endpoint when cart is empty (no such user)
func (s *Suit) TestDeleteAllEmpty() {

	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%s", s.url, deleteAllURL), nil)
	s.Require().NoError(err)
	response, err := s.client.Do(req)
	s.Require().NoError(err)
	defer response.Body.Close()
	s.Require().Equal(http.StatusNoContent, response.StatusCode)
}

// TestDeleteSingleEmpty Testing delete item in cart endpoint when cart is empty (no such user)
func (s *Suit) TestDeleteSingleEmpty() {

	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%s", s.url, deleteSingleURL), nil)
	s.Require().NoError(err)
	response, err := s.client.Do(req)
	s.Require().NoError(err)
	defer response.Body.Close()
	s.Require().Equal(http.StatusNoContent, response.StatusCode)
}
