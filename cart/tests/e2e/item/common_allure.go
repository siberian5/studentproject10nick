package item

import (
	"bytes"
	"fmt"
	"github.com/ozontech/allure-go/pkg/framework/asserts_wrapper/require"
	"github.com/ozontech/allure-go/pkg/framework/provider"
	"github.com/ozontech/allure-go/pkg/framework/suite"
	"io"
	"net/http"
	"os"
)

type AllureSuit struct {
	suite.Suite
	client *http.Client
	url    string
}

func (s *AllureSuit) BeforeAll(_ provider.T) {

	s.client = http.DefaultClient
	s.url = "http://localhost:8082"
}

func (s *AllureSuit) TestHeartbeat(t provider.T) {
	t.Title("Just Heartbeat")
}

// TestGetEmpty Testing get cart endpoint when cart is empty (no such user)
func (s *AllureSuit) TestGetEmpty(t provider.T) {
	t.Parallel()

	t.Title("Get empty cart")
	s.helperGetEmpty(t)
}

// TestDeleteAllEmpty Testing get cart endpoint when cart is empty (no such user)
func (s *AllureSuit) TestDeleteAllEmpty(t provider.T) {
	t.Parallel()

	t.Title("Delete all in empty cart")
	s.helperDeleteAll(t)
}

// TestDeleteSingleEmpty Testing get cart endpoint when cart is empty (no such user)
func (s *AllureSuit) TestDeleteSingleEmpty(t provider.T) {
	t.Parallel()

	t.Title("Delete single in empty cart")
	s.helperDeleteAll(t)
}

func (s *AllureSuit) TestScenario(t provider.T) {
	t.Parallel()

	t.Title("User behaviour imitation")
	t.Description("Scenario where user add items to cart, delete their and check cart")
	t.NewStep("Checking empty cart")

	s.helperGetEmpty(t)

	t.NewStep("Adding to cart existing item")
	s.helperPost(t, "./item/testdata/1_add_to_cart.json", addURL)

	t.NewStep("Get created cart")
	s.helperGetNotEmpty(t, "./item/testdata/2_cart_after_add.json")

	t.NewStep("Adding another to cart")
	s.helperPost(t, "./item/testdata/3_add_another_to_cart.json", addAnotherURL)

	t.NewStep("Get created another cart")
	s.helperGetNotEmpty(t, "./item/testdata/4_cart_after_another_add.json")

	t.NewStep("Delete first from cart")
	s.helperDeleteSingle(t, deleteSingleURL)

	t.NewStep("Get cart afterDelete")
	s.helperGetNotEmpty(t, "./item/testdata/5_cart_after_delete_single.json")

	t.NewStep("Clear cart")
	s.helperDeleteAll(t)

	t.NewStep("Get empty cart")
	s.helperGetEmpty(t)

}

func (s *AllureSuit) helperGetEmpty(t provider.T) {
	response, err := s.client.Get(fmt.Sprintf("%s/%s", s.url, getURL))
	require.NoError(t, err)
	defer response.Body.Close()
	t.Require().Equal(http.StatusNotFound, response.StatusCode)
}

func (s *AllureSuit) helperGetNotEmpty(t provider.T, fname string) {
	response, err := s.client.Get(fmt.Sprintf("%s/%s", s.url, getURL))
	t.Require().NoError(err)
	defer response.Body.Close()
	t.Require().Equal(http.StatusOK, response.StatusCode)

	file, err := os.ReadFile(fname)
	t.Require().NoError(err)
	bodyBytes, err := io.ReadAll(response.Body)
	t.Require().NoError(err)
	t.Assert().Equal(file, bodyBytes, "Not valid json in get cart")
}

func (s *AllureSuit) helperPost(t provider.T, fname, url string) {
	file, err := os.ReadFile(fname)
	t.Require().NoError(err)
	response, err := s.client.Post(fmt.Sprintf("%s/%s", s.url, url), "application/json", bytes.NewReader(file))
	t.Require().NoError(err)
	defer response.Body.Close()
	t.Require().Equal(http.StatusOK, response.StatusCode, "Not success status code")
}

func (s *AllureSuit) helperDeleteSingle(t provider.T, deleteURL string) {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%s", s.url, deleteURL), nil)
	t.Require().NoError(err)
	responseDeleteSingle, err := s.client.Do(req)
	t.Require().NoError(err)
	defer responseDeleteSingle.Body.Close()
	t.Require().Equal(http.StatusNoContent, responseDeleteSingle.StatusCode)
}

func (s *AllureSuit) helperDeleteAll(t provider.T) {
	req, err := http.NewRequest("DELETE", fmt.Sprintf("%s/%s", s.url, deleteAllURL), nil)
	t.Require().NoError(err)
	responseDeleteAll, err := s.client.Do(req)
	t.Require().NoError(err)
	defer responseDeleteAll.Body.Close()
	t.Require().Equal(http.StatusNoContent, responseDeleteAll.StatusCode)
	body, _ := io.ReadAll(responseDeleteAll.Body)
	t.Require().Equal([]byte{}, body)
}
