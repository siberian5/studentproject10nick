package middleware

import (
	"fmt"
	"log/slog"
	"net/http"
)

func Logging(next http.Handler, log *slog.Logger) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		log.Info(fmt.Sprintf("Request with method %s and path %s", r.Method, r.URL.Path))
		next.ServeHTTP(w, r)
	}

	return http.HandlerFunc(fn)
}
