package utils

import (
	"net/http"
)

type RetryRoundTripper struct {
	http.RoundTripper
	n int
}

func NewRetryRoundTripper(n int) *RetryRoundTripper {
	return &RetryRoundTripper{
		RoundTripper: http.DefaultTransport,
		n:            n,
	}
}

func (t *RetryRoundTripper) RoundTrip(req *http.Request) (response *http.Response, err error) {
	for i := 0; i < t.n; i++ {
		response, err = t.RoundTripper.RoundTrip(req)
		if err != nil {
			continue
		}
		// If 420 or 429 try again
		switch response.StatusCode {
		case http.StatusTooManyRequests:
			continue
		case 420:
			continue
		default:
			return
		}
	}

	return
}
