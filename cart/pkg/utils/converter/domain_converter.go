package converter

import "route256.ozon.ru/project/cart/internal/model"
import desc "route256.ozon.ru/project/cart/pkg/api/loms/v1"

func DomainToOrderRequest(userId int64, items []model.SingleItem) *desc.OrderCreateRequest {
	m := make([]*desc.Item, len(items))
	for i, item := range items {
		m[i] = &desc.Item{
			Sku:   item.Id,
			Count: uint32(item.Count),
		}
	}

	return &desc.OrderCreateRequest{
		UserId: userId,
		Items:  m,
	}
}
