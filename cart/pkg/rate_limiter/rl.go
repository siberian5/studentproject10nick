package rate_limiter

import (
	"sync"
	"time"
)

// RateLimiter used for limiting something (ONLY <=1000 rps).
// Like leaky bucket
// It causes by using mutex
type RateLimiter struct {
	mu           sync.Mutex
	tickDuration time.Duration
	last         time.Time
}

func NewRateLimiter(rps uint16) *RateLimiter {
	a := time.Second.Nanoseconds() / int64(rps)
	return &RateLimiter{
		mu:           sync.Mutex{},
		tickDuration: time.Duration(a),
	}
}

func (r *RateLimiter) Take() {
	// lock to multiple access
	r.mu.Lock()
	defer r.mu.Unlock()

	now := time.Now()

	// if its first time
	if r.last.IsZero() {
		r.last = now
		return
	}

	sleepTime := r.tickDuration - now.Sub(r.last)

	// if we must wait
	if sleepTime > 0 {
		time.Sleep(sleepTime)
		r.last = now.Add(sleepTime)
	} else {
		r.last = now
	}

}
