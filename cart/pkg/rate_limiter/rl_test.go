package rate_limiter

import (
	"testing"
)

// Must see something about 100 msec/op
func BenchmarkRL(b *testing.B) {
	rl := NewRateLimiter(10)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		rl.Take()
	}
}
