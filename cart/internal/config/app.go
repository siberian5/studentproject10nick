package config

import "log/slog"

type AppConfig struct {
	Host       string
	Token      string
	LomsDsn    string
	RetryCount int
	Logger     *slog.Logger
}
