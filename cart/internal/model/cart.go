package model

import "route256.ozon.ru/project/cart/internal/client/product"

// SingleItem - Single item not in cart
type SingleItem struct {
	Id    int64  `json:"sku_id"`
	Count uint16 `json:"count"`
}

// CartItem - Single item in cart
type CartItem struct {
	SingleItem
	product.Product
}

// CartInfo - info about cart
type CartInfo struct {
	Items      []CartItem `json:"items"`
	TotalPrice uint32     `json:"total_price"`
}
