package service

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	"route256.ozon.ru/project/cart/internal/repository/cart"
	"route256.ozon.ru/project/cart/internal/service/mocks"
	"testing"
)

func TestCartService_DeleteSingleItem(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	table := []struct {
		name   string
		userId int64
		itemId int64
	}{
		{
			name:   "",
			userId: 1,
			itemId: 1,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.DeleteOneMock.Expect(ctx, tt.userId, tt.itemId).Return(nil)
			err := svc.DeleteSingleItem(ctx, tt.userId, tt.itemId)
			require.NoError(t, err)
		})
	}
}

func TestCartService_ClearCart(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	table := []struct {
		name   string
		userId int64
	}{
		{
			name:   "",
			userId: 1,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.DeleteAllMock.Expect(ctx, tt.userId).Return(nil)
			err := svc.ClearCart(ctx, tt.userId)
			require.NoError(t, err)
		})
	}
}

func TestCartService_DeleteSingleItemError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	table := []struct {
		name    string
		userId  int64
		itemId  int64
		wantErr error
	}{
		{
			name:    "",
			userId:  1,
			itemId:  1,
			wantErr: cart.ErrNoSuchUser,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {
			ctx := context.Background()
			repo.DeleteOneMock.Expect(ctx, tt.userId, tt.itemId).Return(tt.wantErr)
			err := svc.DeleteSingleItem(ctx, tt.userId, tt.itemId)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}

func TestCartService_ClearCartError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	table := []struct {
		name    string
		userId  int64
		wantErr error
	}{
		{
			name:    "",
			userId:  1,
			wantErr: cart.ErrNoSuchUser,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.DeleteAllMock.Expect(ctx, tt.userId).Return(tt.wantErr)
			err := svc.ClearCart(ctx, tt.userId)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}
