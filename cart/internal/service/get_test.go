package service

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	"go.uber.org/goleak"
	"route256.ozon.ru/project/cart/internal/client/product"
	"route256.ozon.ru/project/cart/internal/model"
	"route256.ozon.ru/project/cart/internal/repository/cart"
	"route256.ozon.ru/project/cart/internal/service/mocks"
	"testing"
)

func TestCartService_GetAllItems(t *testing.T) {

	defer goleak.VerifyNone(t)

	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	table := []struct {
		name              string
		userId            int64
		outputDataRepo    []model.SingleItem
		outputDataProduct []product.Product
		want              model.CartInfo
		wantErr           error
	}{
		{
			name:           "No error",
			userId:         1337,
			outputDataRepo: []model.SingleItem{{Id: 1, Count: 10}, {Id: 2, Count: 153}, {Id: 3, Count: 10}},
			outputDataProduct: []product.Product{
				{
					Name:  "First",
					Price: 1,
				},
				{
					Name:  "Second",
					Price: 2,
				},
				{
					Name:  "Third",
					Price: 3,
				},
			},
			want: model.CartInfo{
				Items: []model.CartItem{
					{
						SingleItem: model.SingleItem{
							Id:    1,
							Count: 10,
						},
						Product: product.Product{
							Name:  "First",
							Price: 1,
						},
					},
					{
						SingleItem: model.SingleItem{
							Id:    2,
							Count: 153,
						},
						Product: product.Product{
							Name:  "Second",
							Price: 2,
						},
					},
					{
						SingleItem: model.SingleItem{
							Id:    3,
							Count: 10,
						},
						Product: product.Product{
							Name:  "Third",
							Price: 3,
						},
					},
				},
				TotalPrice: 346,
			},
			wantErr: nil,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.FindAllMock.Expect(ctx, tt.userId).Return(tt.outputDataRepo, nil)
			for _, v := range tt.outputDataRepo {
				productService.GetProductMock.When(minimock.AnyContext, v.Id).Then(tt.outputDataProduct[v.Id-1], nil)
			}
			c, err := svc.GetAllItems(ctx, tt.userId)
			require.NoError(t, err)
			require.Equal(t, tt.want, c)
		})
	}

}

func TestCartService_GetAllItemsError(t *testing.T) {

	t.Parallel()

	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	table := []struct {
		name                string
		userId              int64
		productServiceError error
		repoError           error
		wantErr             error
	}{
		{
			name:                "Product service is down",
			userId:              1,
			productServiceError: product.ErrServiceInternal,
			repoError:           nil,
			wantErr:             product.ErrServiceInternal,
		},
		{
			name:                "Product service is down",
			userId:              1,
			productServiceError: nil,
			repoError:           cart.ErrDbIsDown,
			wantErr:             cart.ErrDbIsDown,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.FindAllMock.Expect(ctx, tt.userId).Return([]model.SingleItem{{Id: 0, Count: 1}}, tt.repoError)
			productService.GetProductMock.Expect(minimock.AnyContext, 0).Return(product.Product{}, tt.productServiceError)
			_, err := svc.GetAllItems(ctx, tt.userId)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}
