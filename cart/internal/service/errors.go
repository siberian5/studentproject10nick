package service

import "errors"

var ErrInsufficientStocks = errors.New("insufficient stocks")
