package service

import (
	"context"
	"route256.ozon.ru/project/cart/internal/model"
	"route256.ozon.ru/project/cart/pkg/rate_limiter"
	"slices"
	"sync"
)

const BatchSize = 4
const RateLimit = uint16(10)

func (s *CartService) GetAllItems(ctx context.Context, userId int64) (model.CartInfo, error) {

	// Find all items from card
	items, err := s.cartRepo.FindAll(ctx, userId)

	// If no such user or cart is empty or something else
	if err != nil {
		return model.CartInfo{}, err
	}

	// Create slice of model.CartItem
	m := make([]model.CartItem, 0, len(items))
	total := uint32(0)

	// Async get product
	var wg sync.WaitGroup
	innerCtx, cancel := context.WithCancel(ctx)
	defer cancel()
	inputCh := make(chan model.SingleItem, len(items))
	outCh := make(chan model.CartItem, len(items))
	errCh := make(chan error, len(items))
	rl := rate_limiter.NewRateLimiter(RateLimit)

	for i := 0; i < BatchSize; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			s.asyncGetFromClient(innerCtx, getFromClientParams{
				cancel:  cancel,
				inputCh: inputCh,
				errCh:   errCh,
				outCh:   outCh,
				rl:      rl,
			})
		}()
	}

	// add all ids to input channel
	for _, v := range items {
		inputCh <- v
	}

	//close input channel
	close(inputCh)
	//waiting all goroutines works done
	wg.Wait()
	// close answers channels
	close(errCh)
	close(outCh)

	// if at least one error in errCh return error
	err, ok := <-errCh
	if ok {
		return model.CartInfo{}, err
	}

	for result := range outCh {
		total += result.Product.Price * uint32(result.Count)
		m = append(m, result)
	}

	// Sort by SKU (id)
	slices.SortFunc(m, func(a, b model.CartItem) int {
		return int(a.Id - b.Id)
	})

	cartInfo := model.CartInfo{
		Items:      m,
		TotalPrice: total,
	}

	return cartInfo, nil
}

// getFromClientParams struct that used for async and rate limited
// requests to product api
type getFromClientParams struct {
	cancel  context.CancelFunc
	inputCh <-chan model.SingleItem
	errCh   chan<- error
	outCh   chan<- model.CartItem
	rl      *rate_limiter.RateLimiter
}

func (s *CartService) asyncGetFromClient(ctx context.Context, p getFromClientParams) {

	for item := range p.inputCh {

		// if inner context is Done(canceled by other goroutines or parent context Done) break loop
		select {
		case <-ctx.Done():
			break
		default:
		}

		// with required rps
		p.rl.Take()

		// getting from client
		product, err := s.productService.GetProduct(ctx, item.Id)
		// if error we need to cancel inner context that other goroutines stop
		if err != nil {
			p.errCh <- err
			p.cancel()
			break
		}
		// if all is ok
		p.outCh <- model.CartItem{
			SingleItem: model.SingleItem{
				Id:    item.Id,
				Count: item.Count,
			},
			Product: product,
		}

	}
}
