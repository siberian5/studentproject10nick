package service

import "context"

func (s *CartService) DeleteSingleItem(ctx context.Context, userId, itemId int64) error {
	return s.cartRepo.DeleteOne(ctx, userId, itemId)
}

func (s *CartService) ClearCart(ctx context.Context, userId int64) error {
	return s.cartRepo.DeleteAll(ctx, userId)
}
