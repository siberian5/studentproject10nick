package service

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/require"
	lomsclient "route256.ozon.ru/project/cart/internal/client/loms"
	"route256.ozon.ru/project/cart/internal/client/product"
	"route256.ozon.ru/project/cart/internal/model"
	"route256.ozon.ru/project/cart/internal/repository/cart"
	"route256.ozon.ru/project/cart/internal/service/mocks"
	"testing"
)

func TestCartService_AddItem(t *testing.T) {
	t.Parallel()
	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	type inputData struct {
		userId int64
		item   model.SingleItem
	}

	table := []struct {
		name      string
		inputData inputData
	}{
		{
			name: "No error",
			inputData: inputData{
				userId: 1,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
		},
		{
			name: "No error",
			inputData: inputData{
				userId: 123,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			productService.GetProductMock.Expect(ctx, tt.inputData.item.Id).Return(product.Product{}, nil)
			loms.GetStocksInfoMock.Expect(ctx, tt.inputData.item.Id).Return(uint64(tt.inputData.item.Count+1), nil)
			repo.CreateMock.Expect(ctx, tt.inputData.userId, tt.inputData.item).Return(nil)
			err := svc.AddItem(ctx, tt.inputData.userId, tt.inputData.item)
			require.NoError(t, err)
		})
	}

}

func TestCartService_AddItemError(t *testing.T) {
	t.Parallel()
	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	type inputData struct {
		userId int64
		item   model.SingleItem
	}

	table := []struct {
		name       string
		inputData  inputData
		productErr error
		repoErr    error
		lomsErr    error
		lomsCount  uint64
		wantErr    error
	}{
		{
			name: "No such product",
			inputData: inputData{
				userId: 1,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
			productErr: product.ErrNoSuchProduct,
			lomsCount:  2,
			repoErr:    nil,
			wantErr:    product.ErrNoSuchProduct,
		},
		{
			name: "Product service is unavailable",
			inputData: inputData{
				userId: 123,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
			lomsCount:  2,
			productErr: product.ErrServiceInternal,
			repoErr:    nil,
			wantErr:    product.ErrServiceInternal,
		},
		{
			name: "Insufficient stocks",
			inputData: inputData{
				userId: 123,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
			productErr: nil,
			lomsErr:    nil,
			lomsCount:  0,
			repoErr:    nil,
			wantErr:    ErrInsufficientStocks,
		},
		{
			name: "Internal loms",
			inputData: inputData{
				userId: 123,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
			productErr: nil,
			lomsErr:    lomsclient.ErrLOMSServiceInternal,
			lomsCount:  0,
			repoErr:    nil,
			wantErr:    lomsclient.ErrLOMSServiceInternal,
		},
		{
			name: "Repository Error",
			inputData: inputData{
				userId: 123,
				item: model.SingleItem{
					Id:    1,
					Count: 1,
				},
			},
			productErr: nil,
			lomsCount:  2,
			repoErr:    cart.ErrDbIsDown,
			wantErr:    cart.ErrDbIsDown,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			productService.GetProductMock.Expect(ctx, tt.inputData.item.Id).Return(product.Product{}, tt.productErr)
			loms.GetStocksInfoMock.Expect(ctx, tt.inputData.item.Id).Return(tt.lomsCount, tt.lomsErr)
			repo.CreateMock.Expect(ctx, tt.inputData.userId, tt.inputData.item).Return(tt.repoErr)
			err := svc.AddItem(ctx, tt.inputData.userId, tt.inputData.item)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}

}

func TestCartService_Checkout(t *testing.T) {
	t.Parallel()
	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	type inputData struct {
		userId int64
	}

	table := []struct {
		name      string
		inputData inputData
	}{
		{
			name:      "No error",
			inputData: inputData{userId: 64},
		},
		{
			name:      "No error",
			inputData: inputData{userId: 1},
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.FindAllMock.Expect(ctx, tt.inputData.userId).Return(nil, nil)
			loms.CreateOrderMock.Expect(ctx, tt.inputData.userId, nil).Return(1, nil)
			repo.DeleteAllMock.Expect(ctx, tt.inputData.userId).Return(nil)
			_, err := svc.Checkout(ctx, tt.inputData.userId)
			require.NoError(t, err)
		})
	}
}

func TestCartService_CheckoutError(t *testing.T) {
	t.Parallel()
	mc := minimock.NewController(t)
	productService := mocks.NewProductServiceMock(mc)
	repo := mocks.NewCartRepoMock(mc)
	loms := mocks.NewLOMSServiceMock(mc)
	svc := NewCartService(repo, productService, loms)

	type inputData struct {
		userId int64
	}

	table := []struct {
		name          string
		inputData     inputData
		cartRepoError error
		LOMSError     error
		ClearError    error
		wantErr       error
	}{
		{
			name:          "No such user",
			inputData:     inputData{userId: 64},
			cartRepoError: cart.ErrNoSuchUser,
			wantErr:       cart.ErrNoSuchUser,
		},
		{
			name:      "No stocks",
			inputData: inputData{userId: 1},
			LOMSError: lomsclient.ErrInsufficientStocks,
			wantErr:   lomsclient.ErrInsufficientStocks,
		},
		{
			name:       "Clear cart error",
			inputData:  inputData{userId: 1},
			ClearError: cart.ErrDbIsDown,
			wantErr:    cart.ErrDbIsDown,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {

			ctx := context.Background()

			repo.FindAllMock.Expect(ctx, tt.inputData.userId).Return(nil, tt.cartRepoError)
			loms.CreateOrderMock.Expect(ctx, tt.inputData.userId, nil).Return(1, tt.LOMSError)
			repo.DeleteAllMock.Expect(ctx, tt.inputData.userId).Return(tt.ClearError)
			_, err := svc.Checkout(ctx, tt.inputData.userId)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}
