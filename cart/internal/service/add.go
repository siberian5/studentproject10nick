package service

import (
	"context"
	"route256.ozon.ru/project/cart/internal/model"
)

func (s *CartService) AddItem(ctx context.Context, userId int64, item model.SingleItem) error {

	// Check if product exists
	_, err := s.productService.GetProduct(ctx, item.Id)
	if err != nil {
		return err
	}

	// Check if we have required stocks
	count, err := s.lomsService.GetStocksInfo(ctx, item.Id)
	if err != nil {
		return err
	}

	if count < uint64(item.Count) {
		return ErrInsufficientStocks
	}

	// Add to cart
	err = s.cartRepo.Create(ctx, userId, item)
	if err != nil {
		return err
	}

	return nil
}

func (s *CartService) Checkout(ctx context.Context, userId int64) (int64, error) {

	// get items from repo
	items, err := s.cartRepo.FindAll(ctx, userId)
	if err != nil {
		return 0, err
	}

	// create order
	id, err := s.lomsService.CreateOrder(ctx, userId, items)
	if err != nil {
		return 0, err
	}

	// clear cart if create order
	err = s.ClearCart(ctx, userId)
	if err != nil {
		return 0, err
	}

	return id, nil
}
