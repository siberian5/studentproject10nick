package service

import (
	"context"
	"route256.ozon.ru/project/cart/internal/client/product"
	"route256.ozon.ru/project/cart/internal/model"
)

//go:generate minimock -i route256.ozon.ru/project/cart/internal/service.CartRepo -s "_mock.go" -o ./mocks/ -g .
type CartRepo interface {
	Create(ctx context.Context, userId int64, item model.SingleItem) error
	FindAll(ctx context.Context, userId int64) ([]model.SingleItem, error)
	DeleteOne(ctx context.Context, userId, itemId int64) error
	DeleteAll(ctx context.Context, userId int64) error
}

//go:generate minimock -i route256.ozon.ru/project/cart/internal/service.ProductService -s "_mock.go" -o ./mocks/ -g .
type ProductService interface {
	GetProduct(ctx context.Context, itemId int64) (product.Product, error)
}

//go:generate minimock -i route256.ozon.ru/project/cart/internal/service.LOMSService -s "_mock.go" -o ./mocks/ -g .
type LOMSService interface {
	CreateOrder(ctx context.Context, userId int64, items []model.SingleItem) (int64, error)
	GetStocksInfo(ctx context.Context, sku int64) (uint64, error)
}

type CartService struct {
	cartRepo       CartRepo
	productService ProductService
	lomsService    LOMSService
}

func NewCartService(repo CartRepo, service ProductService, lomsService LOMSService) *CartService {
	return &CartService{
		cartRepo:       repo,
		productService: service,
		lomsService:    lomsService,
	}
}
