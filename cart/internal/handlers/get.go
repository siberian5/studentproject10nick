package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"log/slog"
	"net/http"
	"route256.ozon.ru/project/cart/internal/client/product"
	"route256.ozon.ru/project/cart/internal/model"
	"strconv"
)

type getCommand interface {
	GetAllItems(ctx context.Context, userId int64) (model.CartInfo, error)
}

func RegisterGetHandler(mux *http.ServeMux, s getCommand, logger *slog.Logger) {
	// Get cart
	mux.HandleFunc("GET /user/{user_id}/cart", func(w http.ResponseWriter, r *http.Request) {

		// Check userId type
		userId, err := strconv.ParseInt(r.PathValue("user_id"), 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Call service method
		cart, err := s.GetAllItems(r.Context(), userId)

		if err != nil {
			// If error when calling other service return 500
			if errors.Is(err, product.ErrServiceInternal) || errors.Is(err, context.DeadlineExceeded) {
				logger.Error(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
			} else {
				// while cart is empty or no such user
				w.WriteHeader(http.StatusNotFound)
			}
			return
		}

		// Marshall struct
		data, err := json.Marshal(cart)
		if err != nil {
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(data)
	})

}
