package handlers

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/gookit/validate"
	"log/slog"
	"net/http"
	"route256.ozon.ru/project/cart/internal/client/loms"
	"route256.ozon.ru/project/cart/internal/client/product"
	"route256.ozon.ru/project/cart/internal/model"
	"route256.ozon.ru/project/cart/internal/repository/cart"
	"route256.ozon.ru/project/cart/internal/service"
	"strconv"
)

type addCommand interface {
	AddItem(ctx context.Context, userId int64, item model.SingleItem) error
}

type checkoutCommand interface {
	Checkout(ctx context.Context, userId int64) (int64, error)
}

func RegisterCreateHandler(mux *http.ServeMux, s addCommand, logger *slog.Logger) {
	// Add item to cart
	mux.HandleFunc("POST /user/{user_id}/cart/{sku_id}", func(w http.ResponseWriter, r *http.Request) {

		// Check content type
		if r.Header.Get("Content-Type") != "application/json" {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Check userId type
		userId, err := strconv.ParseInt(r.PathValue("user_id"), 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Check itemId type
		itemId, err := strconv.ParseInt(r.PathValue("sku_id"), 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		var t AddItemRequest

		// Unmarshall to AddItemRequest
		err = json.NewDecoder(r.Body).Decode(&t)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Validate AddItemRequest
		if v := validate.Struct(&t); !v.Validate() {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Add in service
		err = s.AddItem(r.Context(), userId, model.SingleItem{
			Id:    itemId,
			Count: t.Count,
		})

		if err != nil {
			// If no such product -> return 404
			if errors.Is(err, product.ErrNoSuchProduct) {
				w.WriteHeader(http.StatusNotFound)
				return
			}
			if errors.Is(err, service.ErrInsufficientStocks) {
				w.WriteHeader(http.StatusPreconditionFailed)
				return
			}
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return

		}

		w.WriteHeader(http.StatusOK)
	})

}

func RegisterCheckoutHandler(mux *http.ServeMux, s checkoutCommand, logger *slog.Logger) {
	// Add item to cart
	mux.HandleFunc("POST /cart/checkout", func(w http.ResponseWriter, r *http.Request) {

		var t CheckoutRequest

		// Unmarshall to AddItemRequest
		err := json.NewDecoder(r.Body).Decode(&t)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Validate CheckoutRequest
		if v := validate.Struct(&t); !v.Validate() {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Checkout in service
		id, err := s.Checkout(r.Context(), t.UserId)

		if err != nil {
			// If cart is empty-> return 404
			if errors.Is(err, cart.ErrCartIsEmpty) {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			if errors.Is(err, loms.ErrInsufficientStocks) {
				w.WriteHeader(http.StatusPreconditionFailed)
				return
			}
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return

		}
		// Marshall struct
		data, err := json.Marshal(struct {
			Id int64 `json:"orderID"`
		}{Id: id})

		if err != nil {
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write(data)
	})

}
