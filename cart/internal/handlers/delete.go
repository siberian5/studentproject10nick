package handlers

import (
	"context"
	"errors"
	"log/slog"
	"net/http"
	"route256.ozon.ru/project/cart/internal/repository/cart"
	"strconv"
)

type deleteCommand interface {
	DeleteSingleItem(ctx context.Context, userId, itemId int64) error
	ClearCart(ctx context.Context, userId int64) error
}

func RegisterDeleteHandler(mux *http.ServeMux, s deleteCommand, logger *slog.Logger) {
	// Delete single item in cart
	mux.HandleFunc("DELETE /user/{user_id}/cart/{sku_id}", func(w http.ResponseWriter, r *http.Request) {
		// Check userId type
		userId, err := strconv.ParseInt(r.PathValue("user_id"), 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Check itemId type
		itemId, err := strconv.ParseInt(r.PathValue("sku_id"), 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		//Call service
		err = s.DeleteSingleItem(r.Context(), userId, itemId)
		if err != nil && !errors.Is(err, cart.ErrNoSuchUser) {
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusNoContent)
	})

	// Clear cart
	mux.HandleFunc("DELETE /user/{user_id}/cart", func(w http.ResponseWriter, r *http.Request) {
		// Check userId type
		userId, err := strconv.ParseInt(r.PathValue("user_id"), 10, 64)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Service call
		err = s.ClearCart(r.Context(), userId)
		if err != nil && !errors.Is(err, cart.ErrNoSuchUser) {
			logger.Error(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusNoContent)
	})

}
