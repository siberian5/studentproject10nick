package handlers

type AddItemRequest struct {
	Count uint16 `json:"count" validate:"required|min:1" message:"min:count min value is 1"`
}

type CheckoutRequest struct {
	UserId int64 `json:"user" validate:"required" message:"UserId is required"`
}
