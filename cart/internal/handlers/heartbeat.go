package handlers

import (
	"net/http"
)

func RegisterHearbeatHandler(mux *http.ServeMux) {
	// Heartbeat
	mux.HandleFunc("GET /heartbeat", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

}
