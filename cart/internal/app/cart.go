package app

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"route256.ozon.ru/project/cart/internal/client/loms"
	"route256.ozon.ru/project/cart/internal/client/product"
	"route256.ozon.ru/project/cart/internal/config"
	"route256.ozon.ru/project/cart/internal/handlers"
	"route256.ozon.ru/project/cart/internal/repository/cart/inmemory"
	"route256.ozon.ru/project/cart/internal/service"
	"route256.ozon.ru/project/cart/pkg/middleware"
	"sync"
	"time"
)

type CartApp struct {
	log    *slog.Logger
	server *http.Server
	wg     sync.WaitGroup
}

func NewCartApp(cfg *config.AppConfig) (*CartApp, error) {

	//Get logger
	logger := cfg.Logger

	// Create map repo
	repo := inmemory.NewRepository()

	// Create product service client
	p := product.NewClient("http://route256.pavl.uk:8080", cfg.Token, cfg.RetryCount)

	// Create loms service client
	l, err := loms.NewClient(cfg.LomsDsn)
	if err != nil {
		return nil, err
	}

	// Create service
	svc := service.NewCartService(repo, p, l)

	// Create router
	mux := http.NewServeMux()

	// Register all routes to router
	handlers.RegisterHearbeatHandler(mux)
	handlers.RegisterCreateHandler(mux, svc, logger)
	handlers.RegisterGetHandler(mux, svc, logger)
	handlers.RegisterDeleteHandler(mux, svc, logger)
	handlers.RegisterCheckoutHandler(mux, svc, logger)

	// Create server
	server := &http.Server{
		Addr:         cfg.Host,
		Handler:      http.TimeoutHandler(middleware.Logging(mux, logger), time.Second*1, "timeout"),
		WriteTimeout: time.Second * 1,
		IdleTimeout:  time.Second * 5,
	}

	return &CartApp{
		log:    logger,
		server: server,
		wg:     sync.WaitGroup{},
	}, nil

}

// Start runs Cart application
func (c *CartApp) Start() {

	c.log.Info(fmt.Sprintf("Starting listening at %s", c.server.Addr))

	c.wg.Add(1)
	go func() {
		defer c.wg.Done()
		err := c.server.ListenAndServe()
		if err != nil && !errors.Is(err, http.ErrServerClosed) {
			c.log.Error(err.Error())
			return
		}
	}()
}

// Close used for gs
func (c *CartApp) Close(ctx context.Context) (err error) {

	c.log.Info("Closing server")
	err = c.server.Shutdown(ctx)
	if err != nil {
		c.log.Info(fmt.Errorf("error whiule closing server %w", err).Error())
	}
	c.wg.Wait()

	return
}
