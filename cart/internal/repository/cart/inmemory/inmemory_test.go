package inmemory

import (
	"context"
	"github.com/stretchr/testify/require"
	"go.uber.org/goleak"
	"math/rand"
	"route256.ozon.ru/project/cart/internal/model"
	"route256.ozon.ru/project/cart/internal/repository/cart"
	"sync"
	"testing"
)

func TestMapRepoConcurrent(t *testing.T) {

	goleak.VerifyNone(t)

	r := NewRepository()
	ctx := context.Background()

	wg := sync.WaitGroup{}

	wg.Add(10)

	for i := range 10 {
		go func(k uint16) {

			defer wg.Done()

			//nolint:gosec
			for j := range 10 {
				_ = r.Create(ctx, rand.Int63(), model.SingleItem{
					Id:    rand.Int63(),
					Count: k + uint16(j),
				})

				_, _ = r.FindAll(ctx, rand.Int63())

				_ = r.DeleteOne(ctx, rand.Int63(), rand.Int63())

				_ = r.DeleteAll(ctx, rand.Int63())

			}

		}(uint16(i))
	}

	wg.Wait()

}

func TestMapRepository_Create(t *testing.T) {

	r := NewRepository()

	table := []struct {
		name   string
		userId int64
		item   model.SingleItem
	}{
		{
			name:   "First",
			userId: 23,
			item: model.SingleItem{
				Id:    123,
				Count: 1,
			},
		},
		{
			name:   "Second",
			userId: 1,
			item: model.SingleItem{
				Id:    2,
				Count: 23,
			},
		},
		{
			name:   "Add again",
			userId: 1,
			item: model.SingleItem{
				Id:    2,
				Count: 23,
			},
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			ctx := context.Background()
			err := r.Create(ctx, tt.userId, tt.item)
			require.NoError(t, err)
		})
	}
}

func TestMapRepository_FindAll(t *testing.T) {

	r := NewRepository()
	ctx := context.Background()

	_ = r.Create(ctx, 23, model.SingleItem{
		Id:    1,
		Count: 23,
	})

	_ = r.Create(ctx, 23, model.SingleItem{
		Id:    2,
		Count: 22,
	})

	_ = r.Create(ctx, 24, model.SingleItem{
		Id:    2,
		Count: 22,
	})

	_ = r.Create(ctx, 25, model.SingleItem{
		Id:    2,
		Count: 22,
	})

	_ = r.DeleteOne(ctx, 25, 2)

	table := []struct {
		name    string
		userId  int64
		want    []model.SingleItem
		wantErr error
	}{
		{
			name:    "First",
			userId:  23,
			want:    []model.SingleItem{{Id: 1, Count: 23}, {Id: 2, Count: 22}},
			wantErr: nil,
		},
		{
			name:    "Second",
			userId:  24,
			want:    []model.SingleItem{{Id: 2, Count: 22}},
			wantErr: nil,
		},
		{
			name:    "Error - no such user",
			userId:  1,
			want:    nil,
			wantErr: cart.ErrNoSuchUser,
		},
		{
			name:    "Error - cart is empty",
			userId:  25,
			want:    nil,
			wantErr: cart.ErrCartIsEmpty,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			m, err := r.FindAll(ctx, tt.userId)
			require.Equal(t, tt.wantErr, err)
			require.ElementsMatch(t, m, tt.want)
		})
	}
}

func TestMapRepository_DeleteOne(t *testing.T) {

	r := NewRepository()
	ctx := context.Background()

	_ = r.Create(ctx, 23, model.SingleItem{
		Id:    1,
		Count: 1,
	})

	table := []struct {
		name    string
		userId  int64
		itemId  int64
		wantErr error
	}{
		{
			name:    "First",
			userId:  23,
			itemId:  1,
			wantErr: nil,
		},
		{
			name:    "Second",
			userId:  1,
			itemId:  2,
			wantErr: cart.ErrNoSuchUser,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			err := r.DeleteOne(ctx, tt.userId, tt.itemId)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}

func TestMapRepository_DeleteAll(t *testing.T) {

	r := NewRepository()
	ctx := context.Background()
	_ = r.Create(ctx, 23, model.SingleItem{
		Id:    1,
		Count: 1,
	})

	_ = r.Create(ctx, 23, model.SingleItem{
		Id:    2,
		Count: 1,
	})

	table := []struct {
		name    string
		userId  int64
		wantErr error
	}{
		{
			name:    "First",
			userId:  23,
			wantErr: nil,
		},
		{
			name:    "Second",
			userId:  1,
			wantErr: cart.ErrNoSuchUser,
		},
	}

	for _, tt := range table {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()
			err := r.DeleteAll(ctx, tt.userId)
			require.ErrorIs(t, err, tt.wantErr)
		})
	}
}

func BenchmarkMapRepository_Create(b *testing.B) {

	r := NewRepository()

	ctx := context.Background()

	for i := 0; i < b.N; i++ {
		_ = r.Create(ctx, int64(i), model.SingleItem{
			Id:    int64(i),
			Count: 0,
		})
	}
}

func BenchmarkMapRepository_DeleteOne(b *testing.B) {

	r := NewRepository()

	ctx := context.Background()

	for i := 0; i < b.N; i++ {
		_ = r.Create(ctx, int64(i), model.SingleItem{
			Id:    int64(i),
			Count: 1,
		})
	}
	b.ResetTimer()

	for i := 0; i < b.N; i++ {
		_ = r.DeleteOne(ctx, int64(i), int64(i))
	}
}
