package inmemory

import (
	"context"
	"maps"
	"route256.ozon.ru/project/cart/internal/model"
	cartRepo "route256.ozon.ru/project/cart/internal/repository/cart"
	"sync"
)

type UserItems map[int64]uint16

// Repository - repository to store Users idCarts.
type Repository struct {
	mu      sync.RWMutex // Also can use sync.Map (if concurrency factor >= 4)
	idCarts map[int64]UserItems
}

func NewRepository() *Repository {
	return &Repository{
		mu:      sync.RWMutex{},
		idCarts: make(map[int64]UserItems),
	}
}

func (m *Repository) Create(_ context.Context, userId int64, item model.SingleItem) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	// Get cart from map
	cart, ok := m.idCarts[userId]

	// If no such user just create cart
	if !ok {
		m.idCarts[userId] = UserItems{item.Id: item.Count}
	} else {
		// Don`t need to check if item already in cart (default uint16 = 0)
		cart[item.Id] = cart[item.Id] + item.Count
	}

	return nil
}

func (m *Repository) FindAll(_ context.Context, userId int64) ([]model.SingleItem, error) {
	m.mu.RLock()
	defer m.mu.RUnlock()

	// Get cart from map
	cart, ok := m.idCarts[userId]

	// If no such user just return
	if !ok {
		return nil, cartRepo.ErrNoSuchUser
	}

	// Check if cart is empty
	if len(cart) > 0 {

		out := make([]model.SingleItem, 0, len(cart))

		for k, v := range maps.Clone(cart) {
			out = append(out, model.SingleItem{
				Id:    k,
				Count: v,
			})
		}
		return out, nil
	} else {
		return nil, cartRepo.ErrCartIsEmpty
	}
}

func (m *Repository) DeleteOne(_ context.Context, userId, itemId int64) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	// Get cart from map
	cart, ok := m.idCarts[userId]

	// If no such user just return
	if !ok {
		return cartRepo.ErrNoSuchUser
	}

	// Not necessary if user has that item
	delete(cart, itemId)

	return nil
}

func (m *Repository) DeleteAll(_ context.Context, userId int64) error {
	m.mu.Lock()
	defer m.mu.Unlock()

	// Get cart from map
	cart, ok := m.idCarts[userId]

	// If no such user just return
	if !ok {
		return cartRepo.ErrNoSuchUser
	}

	// Clear user cart
	clear(cart)

	return nil
}
