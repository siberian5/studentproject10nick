package mongo

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"route256.ozon.ru/project/cart/internal/model"
	"route256.ozon.ru/project/cart/internal/repository/cart"
)

type mongoDTO struct {
	UserId int64  `bson:"userId"`
	SkuId  int64  `bson:"skuId"`
	Count  uint16 `bson:"count"`
}

type Repository struct {
	collection *mongo.Collection
}

func (r *Repository) Create(ctx context.Context, userId int64, item model.SingleItem) error {

	result := r.collection.FindOne(ctx, bson.M{"userId": userId, "skuId": item.Id})

	if result.Err() != nil {
		if errors.Is(result.Err(), mongo.ErrNoDocuments) {
			_, err := r.collection.InsertOne(ctx, mongoDTO{
				UserId: userId,
				SkuId:  item.Id,
				Count:  item.Count,
			})
			if err != nil {
				return err
			}

			return nil
		} else {
			return result.Err()
		}
	}

	var c mongoDTO

	err := result.Decode(&c)
	if err != nil {
		return err
	}

	c.Count += item.Count

	_, err = r.collection.UpdateOne(ctx, bson.M{"userId": userId, "skuId": item.Id}, bson.M{
		"$set": c,
	})
	if err != nil {
		return err
	}

	return nil
}

func (r *Repository) FindAll(ctx context.Context, userId int64) ([]model.SingleItem, error) {
	cursor, err := r.collection.Find(ctx, bson.M{"userId": userId})
	if err != nil {
		return nil, err
	}

	var results []mongoDTO

	if err = cursor.All(ctx, &results); err != nil {
		return nil, err
	}

	if len(results) == 0 {
		return nil, cart.ErrCartIsEmpty
	}

	m := make([]model.SingleItem, 0, len(results))

	for _, result := range results {
		m = append(m, model.SingleItem{
			Id:    result.SkuId,
			Count: result.Count,
		})
	}

	return m, nil

}

func (r *Repository) DeleteOne(ctx context.Context, userId, itemId int64) error {
	_, err := r.collection.DeleteOne(ctx, bson.M{"userId": userId, "skuId": itemId})

	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return cart.ErrCartIsEmpty
		} else {
			return err
		}
	}

	return nil

}

func (r *Repository) DeleteAll(ctx context.Context, userId int64) error {
	_, err := r.collection.DeleteMany(ctx, bson.M{"userId": userId})

	if err != nil {
		if errors.Is(err, mongo.ErrNoDocuments) {
			return cart.ErrCartIsEmpty
		} else {
			return err
		}
	}

	return nil
}

func NewRepository(c *mongo.Collection) *Repository {
	return &Repository{c}
}
