package cart

import "errors"

var ErrNoSuchUser = errors.New("no such user")
var ErrCartIsEmpty = errors.New("cart is empty")
var ErrDbIsDown = errors.New("db is down")
