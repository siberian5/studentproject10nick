package product

import "errors"

var ErrNoSuchProduct = errors.New("no such product")
var ErrServiceInternal = errors.New("product service internal")
