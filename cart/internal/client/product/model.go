package product

// Product - model of product
type Product struct {
	Name  string `json:"name" validate:"required|min_len:4" message:"min_len:Name min len value is 4"`
	Price uint32 `json:"price" validate:"required|min:1" message:"min:price min value is 1"`
}

// GetProductRequest - request for GetProduct
type GetProductRequest struct {
	Token  string `json:"token"`
	ItemId int64  `json:"sku"`
}
