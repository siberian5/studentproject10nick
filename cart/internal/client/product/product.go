package product

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/gookit/validate"
	"net/http"
	"route256.ozon.ru/project/cart/pkg/utils"
	"time"
)

type Client struct {
	token      string
	host       string
	retryCount int
}

func NewClient(host, token string, retryCount int) *Client {
	return &Client{
		token:      token,
		host:       host,
		retryCount: retryCount,
	}

}

func (c *Client) GetProduct(ctx context.Context, itemId int64) (p Product, err error) {

	// Create client with retries
	client := http.Client{
		Transport: utils.NewRetryRoundTripper(c.retryCount),
		Timeout:   time.Second * 3,
	}

	// Construct URL
	requestURL := fmt.Sprintf("%s/%s", c.host, "get_product")

	// Construct body
	body := GetProductRequest{
		Token:  c.token,
		ItemId: itemId,
	}

	bodyBytes, err := json.Marshal(&body)
	if err != nil {
		return Product{}, err
	}
	// Execute request
	request, _ := http.NewRequestWithContext(ctx, http.MethodPost, requestURL, bytes.NewReader(bodyBytes))

	response, err := client.Do(request)
	if err != nil {
		return Product{}, err
	}

	defer func() {
		_ = response.Body.Close()
	}()
	// If 404 return error
	if response.StatusCode == http.StatusNotFound {
		return Product{}, ErrNoSuchProduct
	}

	// If 200 return response
	if response.StatusCode == http.StatusOK {

		err = json.NewDecoder(response.Body).Decode(&p)
		if err != nil {
			return Product{}, err
		}

		// Validate the product
		if v := validate.Struct(&p); v.Validate() {
			return
		} else {
			return Product{}, v.Errors
		}

	} else {
		// Other (420,429,etc)
		return Product{}, ErrServiceInternal
	}
}
