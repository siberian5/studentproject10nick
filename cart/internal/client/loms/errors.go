package loms

import "errors"

var ErrLOMSServiceInternal = errors.New("loms service internal")
var ErrInsufficientStocks = errors.New("insufficient stocks")
