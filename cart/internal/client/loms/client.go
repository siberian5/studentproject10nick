package loms

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/status"
	"route256.ozon.ru/project/cart/internal/model"
	desc "route256.ozon.ru/project/cart/pkg/api/loms/v1"
	"route256.ozon.ru/project/cart/pkg/utils/converter"
)

type Client struct {
	conn   *grpc.ClientConn
	client desc.LOMSClient
}

func NewClient(dsn string) (*Client, error) {
	c, err := grpc.Dial(dsn, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	client := desc.NewLOMSClient(c)

	return &Client{
		conn:   c,
		client: client,
	}, nil
}

func (c *Client) Close() error {
	return c.conn.Close()
}

func (c *Client) CreateOrder(ctx context.Context, userId int64, items []model.SingleItem) (int64, error) {
	resp, err := c.client.OrderCreate(ctx, converter.DomainToOrderRequest(userId, items))
	if err != nil {
		if e, ok := status.FromError(err); ok {
			switch e.Code() {
			case codes.FailedPrecondition:
				return 0, ErrInsufficientStocks
			default:
				return 0, fmt.Errorf("%s with code %s and message %s", ErrLOMSServiceInternal.Error(), e.Code().String(), e.Message())
			}
		} else {
			return 0, err
		}
	}
	return resp.OrderId, nil
}

func (c *Client) GetStocksInfo(ctx context.Context, sku int64) (uint64, error) {
	resp, err := c.client.StocksInfo(ctx, &desc.StocksInfoRequest{Sku: sku})
	if err != nil {
		if e, ok := status.FromError(err); ok {
			return 0, fmt.Errorf("%s with code %s and message %s", ErrLOMSServiceInternal.Error(), e.Code().String(), e.Message())
		}

	}

	return resp.Count, nil
}
