package main

import (
	"context"
	"fmt"
	"github.com/spf13/pflag"
	"log"
	"log/slog"
	"os"
	"os/signal"
	"route256.ozon.ru/project/cart/internal/app"
	"route256.ozon.ru/project/cart/internal/config"
	"sync"
	"syscall"
	"time"
)

func main() {

	cfg := parseToConfig()

	// Graceful shutdown
	ctx, stop := signal.NotifyContext(context.Background(), os.Interrupt, syscall.SIGTERM)
	defer stop()

	// creating app
	cart, err := app.NewCartApp(cfg)
	if err != nil {
		log.Fatal(err)
	}

	// create wg to sync graceful shutdown
	var wg sync.WaitGroup

	// shutdown goroutine
	wg.Add(1)
	go func() {
		defer wg.Done()

		// Getting context done from signal.Notify
		<-ctx.Done()
		cfg.Logger.Info("Starting shutdown")

		// Context to close all connections and others
		ctxShutdown, cancel := context.WithTimeout(context.Background(), time.Second*3)
		defer cancel()
		err := cart.Close(ctxShutdown)
		if err != nil {
			slog.Error(err.Error())
		}

		cfg.Logger.Info("Shutdown successful")
	}()

	// start app
	cart.Start()

	// waiting signal.Notify
	wg.Wait()

}

func parseToConfig() *config.AppConfig {

	logger := slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{Level: slog.LevelDebug}))

	// cmd parameters
	var host string
	var token string
	var lomsDsn string
	var retryCount int

	// parse parameters
	pflag.StringVar(&host, "address", "0.0.0.0:8080", "specify a server address")
	pflag.StringVar(&token, "token", "testtoken", "specify a token to product service")
	pflag.StringVar(&lomsDsn, "loms_dsn", "loms:50051", "specify a dsn to loms service")
	pflag.IntVar(&retryCount, "retry_count", 3, "specify a count of retries get info from product service")
	pflag.Parse()

	logger.Debug(fmt.Sprintf("Host: %s", host))
	logger.Debug(fmt.Sprintf("Token: %s", token))
	logger.Debug(fmt.Sprintf("RetryCount: %d", retryCount))
	logger.Debug(fmt.Sprintf("LOMS dsn: %s", lomsDsn))

	return &config.AppConfig{
		Host:       host,
		Token:      token,
		LomsDsn:    lomsDsn,
		RetryCount: retryCount,
		Logger:     logger,
	}

}
