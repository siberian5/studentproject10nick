syntax = "proto3";

package route256.ozon.ru.project.loms.v1;

option go_package = "route256.ozon.ru/project/loms/api/loms/v1;loms";

import "google/protobuf/empty.proto";
import "google/api/annotations.proto";
import "protoc-gen-openapiv2/options/annotations.proto";
import "validate/validate.proto";

option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
  info: {
    title: "Logistics and Order Management System";
    version: "1.0.0";
  };
  schemes: HTTP;
  consumes: "application/json";
  produces: "application/json";
};


service LOMS {
  rpc OrderCreate(OrderCreateRequest) returns (OrderCreateResponse){
    option (google.api.http) = {
      post: "/v1/order"
      body: "*"
    };
  };

  rpc OrderInfo(OrderInfoRequest) returns (OrderInfoResponse){
    option (google.api.http) = {
      get: "/v1/orders/{orderId}"
    };
  };

  rpc OrderPay(OrderPayRequest) returns (google.protobuf.Empty){
    option (google.api.http) = {
      post: "/v1/orders/{orderId}/pay"
    };
  };

  rpc OrderCancel(OrderCancelRequest) returns (google.protobuf.Empty){
    option (google.api.http) = {
      post: "/v1/orders/{orderId}/cancel"
    };
  };

  rpc StocksInfo(StocksInfoRequest) returns (StocksInfoResponse){
    option (google.api.http) = {
      get: "/v1/stocks/{sku}"
    };
  };
}

message Item{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "Item"
      description: "Cart item, that stores sku and count"
      required: ["sku", "count"]
    }
  };
  int64 sku = 1 [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "SKU",
      description: "Unique item id",
      type: INTEGER,
      format: "int64",
      example: "1002"
    },
    (validate.rules).int64.ignore_empty = false
  ];
  uint32 count = 2 [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "Count",
      description: "Count of SKU in cart",
      type: INTEGER,
      minimum: 1,
      format: "uint32",
      example: "10"
    },
    (validate.rules).uint32.gte = 1,
    (validate.rules).uint32.ignore_empty = false
  ];
}

message OrderCreateRequest{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "OrderCreateRequest"
      description: "Request to create order"
      required: ["userId", "items"]
    }
  };
  int64 userId = 1 [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "userId",
      description: "User id",
      type: INTEGER,
      format: "int64",
      example: "10"
    },
    (validate.rules).int64.ignore_empty = false
  ];
  repeated Item items = 2[
    (validate.rules).repeated.min_items = 1
  ];
}

message OrderCreateResponse{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "OrderCreateResponse"
      description: "Response on success create order"
    }
  };
  int64 orderId = 1 [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "orderId",
      description: "Order id",
      type: INTEGER,
      format: "int64",
      example: "10"
    }
  ];
}

message OrderInfoRequest {
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "OrderInfoRequest"
      description: "Request to get order info"
      required: ["orderId"]
    }
  };
  int64 orderId = 1  [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "orderId",
      description: "Order id",
      type: INTEGER,
      format: "int64",
      example: "10"
    },
    (validate.rules).int64.ignore_empty = false
  ];
}

message OrderInfoResponse {

  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "OrderInfoResponse"
      description: "Response by getting order info"
    }
  };

  string status = 1 [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "status",
      description: "Order status",
      type: STRING,
      example: "\"payed\""
    }
  ];
  int64 userId = 2 [
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "userId",
      description: "User id",
      type: INTEGER,
      format: "int64",
      example: "1"
    }
  ];
  repeated Item items = 3;
}

message OrderPayRequest{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "OrderPayRequest"
      description: "Request to handle payed order"
    }
  };
  int64 orderId = 1[
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "orderId",
      description: "Order Id",
      type: INTEGER,
      format: "int64",
      example: "1"
    },
    (validate.rules).int64.ignore_empty = false
  ];
}

message OrderCancelRequest{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "OrderCancelRequest"
      description: "Request to handle canceled order"
    }
  };
  int64 orderId = 1[
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "orderId",
      description: "Order Id",
      type: INTEGER,
      format: "int64",
      example: "1"
    },
    (validate.rules).int64.ignore_empty = false
  ];
}

message StocksInfoRequest{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "StocksInfoRequest"
      description: "Request to get available stocks"
    }
  };
  int64 sku = 1[
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "SKU",
      description: "Unique id for item",
      type: INTEGER,
      format: "int64",
      example: "1"
    },
    (validate.rules).int64.ignore_empty = false
  ];
}

message StocksInfoResponse{
  option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_schema) = {
    json_schema: {
      title: "StocksInfoResponse"
      description: "Response to get available stocks"
    }
  };
  uint64 count = 1[
    (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {
      title: "count",
      description: "count of available stock for item",
      type: INTEGER,
      format: "uint64",
      example: "1"
    }
  ];
}