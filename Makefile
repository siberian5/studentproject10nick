.EXPORT_ALL_VARIABLES:
export POSTGRESQL_MASTER_PASSWORD=password123
export POSTGRESQL_SLAVE_PASSWORD=password123
export POSTGRESQL_REPL_USER=repl_user
export POSTGRESQL_REPL_PASSWORD=repl_password
export PGADMIN_DEFAULT_EMAIL=test@mail.com
export PGADMIN_DEFAULT_PASSWORD=password123

build-all:
	cd cart && GOOS=linux GOARCH=amd64 make build
	cd loms && GOOS=linux GOARCH=amd64 make build

run-all: build-all
	docker-compose up --force-recreate --build
